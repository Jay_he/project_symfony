<?php

namespace ApiBundle\Controller;

use Manages\Solution\Sms\Third\TianYi;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PublicController extends ApiController
{
    /**
     * 服务器数据包
     *
     * @return array|bool|object|\Symfony\Component\HttpFoundation\Response
     */
   public function serverDataAction()
   {
       $r = $this->inlet('ajax', false);
       if($r !== true){
           return $r;
       }

       $data = [];

       return $this->response($this->getParameter('message_success_get_data'), 0, $data);
   }

    /**
     * 发送短信
     *
     * @return array|bool|JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
   public function sendSmsAction()
   {
       $type = self::$s_request->get('type');
       $phone = self::$s_request->get('phone');

       self::$s_verify->isMobile($phone);

       if(self::$s_verify->getError()){
           return $this->response(self::$s_verify->getError());
       }

       switch ($type){
           case 1:  //用户端注册
           default:
               self::$s_verify->setError("类型溢出");
       }


       if(self::$s_verify->getError()){
           return $this->response(self::$s_verify->getError());
       }

       $sms = new TianYi($this->container);

       return new JsonResponse($sms->sendPhoneCode($phone, $type, 0));
   }


    /**
     * 测试地址
     *
     * @return Response
     */
    public function betaAction()
    {
        if($_GET['pwd'] != 'dasdasfasda'){
            throw  new NotFoundHttpException();
        }

        return new Response('这是一个测试地址');
    }

    /**
     * 日志地址
     */
    public function logAction()
    {
        if($_GET['pwd'] != 'dsahdj'){
            throw  new NotFoundHttpException();
        }

        $this->get('services.files')->readLog($_GET['file'], $_GET['rows']);

        exit;
    }
}
