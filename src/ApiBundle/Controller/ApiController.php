<?php

namespace ApiBundle\Controller;

use Manages\ProjectController;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiController extends ProjectController
{
    static $client = null;  //客户端

    public function __construct(ContainerInterface $container = null)
    {
        parent::__construct($container);
        if(empty($this->getPlatform())) {
            $this->setPlatform(isset($_GET['platform']) ? ($_GET['platform']) : (isset($_POST['platform']) ? $_POST['platform'] : NULL));
        }
        if(in_array($this->getPlatform(), array('Android', 'Ios'))){
            if(!array_key_exists('token', $_GET) || !isset($_GET['version'])){
                exit('缺少必填参数');
            }
            $this->setToken(isset($_GET['token']) ? $_GET['token'] : null);
            $this->setVersion($_GET['version']);
        }
        if(empty(self::$client)){
           self::$client = isset($_GET['client']) ? ($_GET['client']) : (isset($_POST['client']) ? $_POST['client'] : NULL);
        }

        $this->setReturnType('ajax');
    }
}
