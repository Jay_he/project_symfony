<?php

namespace ManagesBundle\Controller;

use Manages\Model\AbstractModel;
use Manages\Model\Model\m_user;
use Manages\ProjectController;
use Manages\Solution\Setting\Setting;
use Manages\Solution\Sms\Third\ChaoXin;
use Manages\Solution\Sms\Third\TianYi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends ProjectController
{

    public function __construct(ContainerInterface $container = null)
    {
        parent::__construct($container);

        $this->setReturnType('ajax');
    }

    public function indexAction()
    {
        $m_user = new m_user($this->container);
        $data['key'] = self::$s_request->get('key', '');
        $data['val'] = self::$s_request->get('val', '');
        $data['order_by'] = 'sql_pre.id DESC';
        self::$s_request->set('rows', 5);
        $data = array_merge($m_user->getList($data), $data);
        $data['paging'] = self::$s_admin->paging($data['count']);
        $data['cmt_arr'] = explode(', ', $m_user->getSqlArray()['select']);
        self::$s_admin->setUrl('user');

        return $this->render('@Manages/Default/index.html.twig', array('data' => $data));
    }

    public function infoAction()
    {
        $m_user = new m_user($this->container);
        $data['id'] = self::$s_request->get('id', 0);
        $data['info'] = $m_user->getInfo($data['id']);
        $data['cmt_arr'] = explode(', ', $m_user->getSqlArray()['select']);

        return $this->render('@Manages/Default/info.html.twig', array('data' => $data));
    }

    public function updateAction()
    {
        $m_user = new m_user($this->container);

        return new JsonResponse($m_user->insertOrUpdate(self::$s_request->get('id', 0)));
    }

    public function delAction()
    {
        $m_user = new m_user($this->container);
        $m_user->delete(self::$s_request->get('id'));

        $this->goUrl(self::$s_admin->getAdminReturn());
    }

    public function settingListAction()
    {
        $m_s = new Setting($this->container);
        $data['info'] = $m_s->getList();
        self::$s_admin->setUrl('setting');

        return $this->render('@Manages/Default/settionList.html.twig', array('data' => $data));
    }

    public function settingInfoAction()
    {
        $m_s = new Setting($this->container);
        $data['i'] = $this->get('utils.requests')->get('item');
        $data['info'] = $m_s->getInfo($data['i']);

        return $this->render('@Manages/Default/settingInfo.html.twig', array('data' => $data));
    }

    public function updateSettingAction()
    {
        $m_s = new Setting($this->container);

        return new JsonResponse($m_s->setValue($this->get('utils.requests')->get('i')));
    }

    public function betaAction()
    {
        return $this->render('@Base/admin/example/list.html.twig');
    }


    public function ccAction()
    {
        return $this->render('@Manages/Default/cc.html.twig');
    }
}
