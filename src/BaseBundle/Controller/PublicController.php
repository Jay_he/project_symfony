<?php
namespace BaseBundle\Controller;

/**
 * 说明
 *
 * 该Banner不允许修改和集成  由框架进行维护
 *
 * 该Banner存在Entity代码以及根据加载的解决方案提供不同的公共免开发维护API
 *
 * 该控制器下的Entity目录可以进行进行修改编辑
 */

use Gregwar\Captcha\CaptchaBuilder;
use Manages\ProjectController;
use Manages\Servers\InputOutPut\Responses;
use Manages\Solution\Area\Area;
use Symfony\Component\HttpFoundation\Response;

class PublicController extends ProjectController
{
    /**
     * 生成二维码控制器
     *
     * @return Response
     */
    public function qrCodeAction()
    {
        $siteUrl = urldecode($this->get('utils.requests')->get('site_url'));
        $this->get('services.images')->qrcode($siteUrl);

        return new Response();
    }


    /**
     * 图形验证码控制器
     *
     * @return Response
     */
    public function captchaAction()
    {
        $type = $this->get('utils.requests')->get('type');

        $r = '';

        $builder = new CaptchaBuilder;
        $builder->build();
        header('Content-type: image/jpeg');

        if(self::$s_request->get('platform') == $this->getParameter('from_android')){
            $r = $builder->inline();
        }else{
            $builder->output();
        }

        $this->get('services.sessions')->set(!empty($type) ?  $type : 'captcha' , $builder->getPhrase());


        return new Response($r);
    }

    /**
     * 上传文件资源控制器
     *
     * @return array|Response
     */
    public function uploadResourcesAction()
    {
        $data = json_encode($this->get('services.files')->upload());

        //此处理为了兼容低版本
        if($this->getPlatform() == $this->getParameter('from_frontend')) {
            return $this->render('@Base/public/json.html.twig', array('data' => $data));
        }else{
            return new Response($data);
        }
    }

    /**
     * 地址联动选择
     *
     * @return array|bool|object|\Symfony\Component\HttpFoundation\Response
     */
    public function areaAction()
    {

        $area = new Area($this->container);

        $parent_id = self::$s_request->get('parent_id', 0);

        $data['info'] =$area->getSonArea($parent_id);

        return $this->response($this->getParameter('message_success_get_data'), 0, $data);
    }


    /**
     * 得到全部的省市区
     *
     * @return array|bool|object|\Symfony\Component\HttpFoundation\Response
     */
    public function allAreaAction()
    {
        $r = $this->inlet('ajax', false);
        if($r !== true){
            return $r;
        }

        $area = new Area($this->container);

        $data['info'] =  $area->getAll();

        return $this->response($this->getParameter('message_success_get_data'), 0, $data);
    }
}
