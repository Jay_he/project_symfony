<?php
namespace BaseBundle\Controller;

/**
 * 说明
 *
 * 该Banner不允许修改和集成  由框架进行维护
 *
 * 该Banner存在Entity代码以及根据加载的解决方案提供不同的公共免开发维护API
 *
 * 该控制器下的Entity目录可以进行修改编辑
 */

use Manages\ProjectController;
use Manages\Solution\ErrorOdr\LoginErrorReturn;

class ManagesController extends ProjectController
{
    public function postSubmitHtmlAction($key)
    {
        $ler = new LoginErrorReturn($this->container);
        return $this->render('@Base/manages/postSubmitHtml.html.twig', array('data' => $ler->callback($key)));
    }
}
