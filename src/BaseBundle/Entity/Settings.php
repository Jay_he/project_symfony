<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Settings
 *
 * @ORM\Table(name="settings", options={"comment":"系统配置表"})
 * @ORM\Entity
 */
class Settings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="config_key", type="string", length=255, unique=true, options={"comment":"键"})
     */
    private $configKey;

    /**
     * @var string
     *
     * @ORM\Column(name="config_value", type="text", nullable=true, options={"comment":"值"})
     */
    private $configValue;

    /**
     * @var string
     *
     * @ORM\Column(name="value_type", type="string", options={"comment":"值类型"})
     */
    private $valueType;

}
