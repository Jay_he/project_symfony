<?php
/**
 * 设置表内置数据
 */
namespace BaseBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Manages\ProjectController;
use Manages\Solution\Admin\Admin;
use Manages\Solution\Area\Area;
use Manages\Solution\Doc\Sql;
use Manages\Solution\Setting\Setting;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadTableData extends ProjectController  implements FixtureInterface, ContainerAwareInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        //settings
        self::$conn = $this->container->get('database_connection');
        self::$s_verify = $this->container->get('services.validations');
        self::$s_sql = new Sql($this->container);
        self::$s_admin = new Admin($this->container);

        $settings = new Setting($this->container);
        $settings->builtInData();
        $area = new Area($this->container);
        $area->builtInData();
    }

}