<?php
/**
 * 变量
 */

namespace Manages\App;

class Variable
{
    /**
     * 通常情况下需ajax返回的来源 --为fromSetReturnType()提供支持
     *
     * @var array
     */
    public static $return_ajax_from  = array('Android', 'Ios');

    public static $gender_array = array(1 => '男', 2 => '女');

    public static $enabled_array = array(0 => '禁用', 1 => '开启');
    
    public static $payment_array = array(1 => '支付宝', 2 => '微信', 3 => '银联' , 4 => '余额');
}