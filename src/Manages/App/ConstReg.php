<?php
/**
 * 常量正则
 *
 * User: Jay
 * DateTime: 7/4/171:17 AM
 */

namespace Manages\App;

class ConstReg
{
    /**
     * 正则 邮箱
     */
    const REG_EMAIL = '/^[^\@]+@.*\.[a-z]{2,6}$/i';

    /**
     * 正则 手机号码
     */
    const REG_MOBILE = '/^1[34578]{1}\d{9}$/';

    /**
     * 正则 WX号
     */
    const REG_WX_NUMBER = '/^[a-zA-Z0-9_-]{5,19}$/';

    /**
     * 正则 座机号码
     */
    const REG_TELEPHONE = '/^0\d{2,3}-[1-9]\d{6,7}$/';

    /**
     * 正则 QQ
     */
    const REG_QQ = '/^[1-9]\d{4,10}$/';

    /**
     * 正则 价格:2位小数
     */
    const REG_PRICE = '/^[0-9]+(.[0-9]{1,2})?$/';

    /**
     * 正则 密码
     */
    const REG_PASSWORD = '/^[\\~!@#$%^&*()-_=+|{}\[\],.?\/:;\'\"\d\w]{6,20}$/';

    /**
     * 正则 名称
     */
    const REG_NAME = '/^[0-9a-zA-Zxa0-xff_]$/';

    /**
     * 正则 经度
     */
    const REG_LNG = '/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}$/';

    /**
     * 正则 维度
     */
    const REG_LAT = '/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/';

    /**
     * 正则 银行卡
     */
    const REG_BANK_CARD    = '/^(\d{16}|\d{19})$/';

    /**
     * 正则 联系方式 (手机号以及座机号验证正则)
     */
    const REG_CONTACT_WAY = '/^(0\d{2,3}-[1-9]\d{6,7})|(1[34578]{1}\d{9})$/';

    /**
     * 正则 邮政编码
     */
    const REG_POSTCODE = '/^[1-9][0-9]{5}$/';

    /**
     * 正则 身份证号码
     */
    const REG_ID_CARD_NO = '/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/';
}