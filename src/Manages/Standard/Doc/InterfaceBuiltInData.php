<?php

/**
 * Created by PhpStorm.
 * User: Jay
 * DateTime: 7/6/171:10 AM
 */
namespace Manages\Standard\Doc;

interface InterfaceBuiltInData
{
    public function builtInData();
}