<?php

/**
 * 实体类接口
 */
namespace Manages\Standard\Model;

interface InterfaceModel 
{
    /**
     * 设置info
     *
     * @param $info
     * @return mixed
     */
    public function setInfo($info);

    /**
     * insert/update
     *
     * @param int $id  0->install >0->
     * @param $data
     * @return mixed
     */
    public function insertOrUpdate($id = 0, $data = []);

    /**
     * 删除
     *
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * 得到列表
     *
     * @param array $rule
     * rule['select'] : 字段规则
     * rule['where'] : where规则
     * rule['order_by'] : order_by规则
     * @return mixed
     */
    public function getList(array $rule = array());

    /**
     * 得到详情
     *
     * @param $id
     * @param array $rule
     * rule['select'] : 字段规则
     * rule['where'] : where规则
     * @return mixed
     */
    public function getInfo($id, array $rule = array());

    /**
     * 规则
     *
     * @param array $rule
     * @param string $id
     * @return mixed
     */
    public function rule($rule = array(), $id = '');
    
}