<?php

namespace Manages\Standard\ThirdParty;
/**
 * 第三方接口规则
 *
 * Interface InterfaceAdorner
 */
interface InterfaceThirdParty
{
    /**
     * 返回值重写
     *
     * @param $return
     * @return mixed
     */
    public function returnRewrite($return);
}