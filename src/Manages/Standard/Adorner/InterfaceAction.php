<?php
/**
 * Action 装饰器
 * 
 * User: zmit
 * Date: 7/3/17
 * Time: 12:58 AM
 */

namespace Manages\Standard\Adorner;



use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

interface InterfaceAction
{
    public function onKernelController(FilterControllerEvent $event);
}