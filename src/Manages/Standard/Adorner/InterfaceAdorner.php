<?php

namespace Manages\Standard\Adorner;
/**
 * 装饰器接口
 *
 * Interface InterfaceAdorner
 */
interface InterfaceAdorner
{
    /**
     * 之前装饰
     *
     * @return mixed
     */
    public function before();

    /**
     * 之后装饰
     *
     * @return mixed
     */
    public function after();
}