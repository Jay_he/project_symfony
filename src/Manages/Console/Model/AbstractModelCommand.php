<?php

namespace Manages\Console\Model;


use Manages\Console\AbstractCommand;

class AbstractModelCommand extends AbstractCommand
{
    protected function getModelPath($entityClassName)
    {

    }

    /**
     * 得到Entity类的完整路径
     *
     * @param $entityClassName
     * @return string
     */
    protected function getEntityClassPath($entityClassName)
    {
        return $this->dir . '/BaseBundle/Entity/' . $entityClassName . '.php';
    }
}