<?php
/**
 * 生成Model
 */

namespace  Manages\Console\Model;

use Manages\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateModelCommand extends AbstractModelCommand
{
    protected function configure()
    {
        $this
            ->setName('manages:generate:model')
            ->setDescription('生成实体类代码')
            ->setHelp('生成实体类代码')
            ->addArgument('entity_class_name', InputArgument::OPTIONAL, 'Entity类名')
            ->addOption('all', 'a', InputOption::VALUE_NONE, '全部的Entity都生成实体代码');

        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io->title($this->getDescription());

        if(empty($input->getOption('all'))){
            $entityClassName = trim($input->getArgument('entity_class_name'));

            while (empty($entityClassName)) {
                $entityClassName = trim($this->io->ask('EntityClassName'));
            }
        }



    }
}