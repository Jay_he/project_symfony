<?php
/**
 * Action之前装饰器 --用于自动加载实例化项目基础类
 *
 * User: zmit
 * Date: 7/3/17
 * Time: 1:05 AM
 */

namespace Manages\Servers\Mechanism;


use Manages\MastersController;
use Manages\Solution\Admin\Admin;
use Manages\Solution\Doc\Sql;
use Manages\Standard\Adorner\InterfaceAction;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class AdornerActionLoadClass extends MastersController implements InterfaceAction
{
    public function onKernelController(FilterControllerEvent $event)
    {
        // TODO: Implement onKernelController() method.
        self::$s_request = $this->get('utils.requests');
        if(self::$conn === null){
            //TODO 实例化基础服务类
            self::$conn = $this->get('database_connection');
            self::$s_verify = $this->get('services.validations');
            self::$s_sql = new Sql($this->container);
            self::$s_admin = new Admin($this->container);
            //TODO 基础参数载入
            self::$s_request->set('session_return_url_name', $this->get('services.sessions')->getName(self::$s_admin->session_return_url));
        }
    }
}