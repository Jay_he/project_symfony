<?php
/**
 * Action之前装饰器 --用于对Request参数进行封装及集成
 *
 * User: zmit
 * Date: 7/3/17
 * Time: 1:05 AM
 */

namespace Manages\Servers\Mechanism;


use Manages\Servers\InputOutPut\Requests;
use Manages\Standard\Adorner\InterfaceAction;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class AdornerActionRequest implements InterfaceAction
{
    private $container = null;

    public function __construct(ContainerInterface $container )
    {
        $this->container = $container;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        // TODO: Implement onKernelController() method.
        //Request封装逻辑梳理  utils.requests
        //!1:框架共识 全面用 utils.requests  替换原生的requests [[一切便利都建立在共识之上]]
        //!2:在twig调用子控制器的时候需要将requests同步过来,但是强制同步，会造成utils.requests 的数据丢失
        //!3:在第一次进入控制器的时候我们需要将request中的值同步的utils.requests，然后保持对象，不断的进行get_set操作
        if(empty(Requests::$request)){
            Requests::$request = $this->container->get('request');
        }

        if(count($this->container->get('request')->attributes->all()) > 3) {
            //!4:为解决！2的放弃强制赋值，拿出数据对其进行set更新操作
            $data = $this->container->get('request')->attributes->all();
            //!!!! data中恒包含_format,_format,_controller 对于这三个值中的_controller可以同步，也可以不同步,默认选择不进行同步[这句注释和执行有错误，代码并没有按照注释执行]
            foreach ($data as $index => $value){
                if(!in_array($index, array('_format', '_format', '_controller'))){
                    Requests::$request->attributes->set($index, $value);
                }
            }
        }
    }
}