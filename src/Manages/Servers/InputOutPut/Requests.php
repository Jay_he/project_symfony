<?php

/**
 * Request封装
 *
 */

namespace Manages\Servers\InputOutPut;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Manages\Servers\Tool\Strings;

class Requests
{
    /**
     * request 对象
     *
     * @var null|object|\Symfony\Component\HttpFoundation\Request
     */
    public static $request = null;

    /**
     * get对象
     *
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     */
    private $get;

    /**
     * post 对象
     *
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     */
    private $post;

    /**
     * attributes 对象
     *
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     */
    private $attributes;

    /**
     * container 对象
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * 初始化
     *
     * Requests constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->get = self::$request->query;
        $this->attributes = self::$request->attributes;
        $this->post = self::$request->request;
        $this->container = $container;
    }

    /**
     * request  限制
     *
     * @param $key
     * @param $value
     * @param bool $is_saved  是否允许表情通过(表情包含img) | 用过的表情在输出的时候都需要转义
     * @return mixed
     */
    public function requestHandle($key, $value, $is_saved = false)
    {
        //过滤掉两边的空格
        if(!is_array($value)) {
            $value = trim($value);

            // 对两位金额进行限制
            if (is_numeric($value) && strpos($value, '.') !== false) {
                if ($value > 0) {
                    if (round($value, 2) <= 0) {
                        $value = 0;
                    }
                }
            }

            //对page和rows进行限制
            if ($key == 'page' || $key == 'rows') {
                if (!(int)$value) {
                    $value = 1;
                }
                if ($value < 1) {
                    $value = 1;
                }
            }

            if ($is_saved == false) {
                //对表情进行限制--过滤掉表情
                $String = new Strings();
                $value = $String->encodeContentWithEmoticon($value, $is_saved);
            }
        }

        return $value;
    }

    /**
     * 得到request中的值
     *
     * @param $key
     * @param null $default
     * @param bool $is_saved
     * @param int $d_bug  是否测试
     * @return mixed|null
     */
    public function get($key, $default = null, $is_saved = false , $d_bug = 0)
    {
        $value = self::$request->get($key, $default);

        $value = $this->requestHandle($key, $value, $is_saved);

        return empty($value) && (string)$value !== '0' ? $default : $value;
    }

    /**
     * 得到get中的值
     *
     * @param $key
     * @param null $default
     * @param bool $is_saved
     * @return mixed|null
     */
    public function getGet($key, $default = null, $is_saved = false)
    {
        $value = $this->get->get($key, $default);

        $value = $this->requestHandle($key, $value, $is_saved);

        return empty($value) && (string)$value !== '0' ? $default : $value;
    }

    /**
     * 得到post中的值
     *
     * @param $key
     * @param null $default
     * @param bool $is_saved
     * @return mixed|null
     */
    public function getPost($key, $default = null, $is_saved = false)
    {
        $value = $this->post->get($key, $default);

        $value = $this->requestHandle($key, $value, $is_saved);

        return empty($value) && (string)$value !== '0' ? $default : $value;
    }

    /**
     * 得到post中的全部值
     *
     * @return array
     */
    public function getPostAll()
    {
        return $this->post->all();
    }

    /**
     * 得到get中的全部值
     *
     * @return array
     */
    public function getGetAll()
    {
        return $this->get->all();
    }

    /**
     * 设置request中的值
     *
     * @param $key
     * @param $value
     * @param string $request
     */
    public function set($key, $value, $request = 'query')
    {
        self::$request->{$request}->set($key, $value);
    }

    /**
     * 设置set中的值
     *
     * @param $key
     * @param $value
     */
    public function setGet($key, $value)
    {
        $this->set($key, $value, 'query');
    }

    /**
     * 设置post中的值
     *
     * @param $key
     * @param $value
     */
    public function setPost($key, $value)
    {
        $this->set($key, $value, 'request');
    }


    /**
     * 获得上一级url
     *
     * @return array|string
     */
    public function getForwardUrl()
    {
        return self::$request->headers->get('referer');
    }

    /**
     * 得到当前的URL
     *
     * @return string
     */
    public function getCurrentRoute()
    {
        return 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    /**
     * 获得当前路由名
     *
     * @return mixed
     */
    public function getCurrentRouteName()
    {
        return self::$request->get('_route');
    }


    /**
     * 获得主机著名(域名)
     *
     * @return string
     */
    public function getHost()
    {
        return self::$request->getSchemeAndHttpHost();
    }

    /**
     * 获得ip地址
     *
     * @return string
     */
    public function getIp()
    {
        return self::$request->getClientIp();
    }


}
