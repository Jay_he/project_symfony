<?php

/**
 * 过滤处理
 */

namespace  Manages\Servers\Tool;

class Filters
{

    /**
     * 过滤Emoji表情
     *
     * @param $content
     * @return mixed
     */
    public function emoji($content)
    {
        return preg_replace_callback(
            '/./u',
            function (array $match) {
                return strlen($match[0]) >= 4 ? '' : $match[0];
            },
            $content
        );
    }

    /**
     * 过滤空格/回车
     *
     * @param $str
     * @return mixed
     */
    public function space($str)
    {
        return str_replace(array(" ","　","\t","\n","\r") , array("","","","",""),$str);
    }

    /**
     *  过滤引号
     *
     * @param $str
     * @return mixed
     */
    public function quotes($str)
    {
        return str_replace(array("'",'"') , array("",""),$str);
    }
    
}
