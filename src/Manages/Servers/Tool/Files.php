<?php

/**
 * 文件处理
 *
 * 维护： Jay
 * 方向： 使项目物理资源完全控制
 * 最后更新日期： 2017-12-05
 * 注意：
 *      1>全部的上传资源都保存在update目录下
 *      2>对上传资源的名称进行统一的规定和支持
 */

namespace  Manages\Servers\Tool;

use Gregwar\Image\Image;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Files
{
    protected $file_types = array(
        //图片
        0 => array(
            'types' => array('image/png', 'image/gif', 'image/jpg', 'image/jpeg', 'image/x-icon'),
            'size' => '10M',
            'title' => '图片'
        ),
        //视频
        1 => array(
            'types' => array('video/mp4','video/x-msvideo','video/3gpp'),
            'size' => '100M',
            'title' => '视频'
        ),
        //音频
        2 => array(
            'types' => array('audio/mpeg'),
            'size' => '100M',
            'title' => '音频'
        ),
        //文件
        3 => array(
            'types' => array('application/msword', 'application/vnd.ms-excel', 'application/vnd.ms-powerpoint',
                'application/pdf', 'application/octet-stream','application/doc','application/zip','application/excel',
                'application/rar','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
            'size' => '100M',
            'title' => '文件'
        ),
        //压缩包
        4 => array(
            'types' => array('application/zip', 'application/rar'),
            'size' => '100M',
            'title' => '压缩文件'
        ),
        //表格
        5 => array(
            'types' => array('application/vnd.ms-excel','application/excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
            'size' => 100,
            'title' => '表格'
        )
    );


    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Validations
     */
    private $validations;

    /**
     * 初始化
     *
     * Files constructor.
     * @param ContainerInterface $container
     * @param Validations $validations
     */
    public function __construct(ContainerInterface $container, $validations)
    {
        $this->container = $container;
        $this->validations = $validations;
    }

    /**
     * 获取数据源
     *
     * @param $fileName
     * @return mixed
     */
    public function readToJson($fileName)
    {
        $fileName = dirname( $this->container->get('kernel')->getRootDir() ). DIRECTORY_SEPARATOR .$fileName;

        $file = fopen($fileName, 'r');
        $data = fread($file, filesize($fileName));
        $data = json_decode($data, true);
        fclose($file);

        return $data;
    }

    /**
     * 文件默认保存路径
     *
     * @return false|string
     */
    public function getDefSavePath()
    {
        return date('Y-m');
    }

    /**
     * 文件默认名称
     *
     * @return string
     */
    public function getDefFileName()
    {
        return uniqid();
    }

    /**
     * 创建路径
     *
     * @param $path
     */
    public function mkdirPath($path)
    {
        if(!file_exists($path)) {
            $dirs = explode('/' , $path);
            $count = count($dirs);
            $path = '.';
            for ($i = 0; $i < $count; ++$i) {
                $path .= '/' . $dirs[$i];
                if(!is_dir($path)){//不是目录
                    if(!mkdir($path,0755)){
                    }
                }
            }
        }
    }

    /**
     * 上传文件
     *
     * @param $inputName
     * @param $file_type
     * @param string $path
     * @param null $filename
     * @return array
     */
    public function upload($path = 'upload',  $filename = null)
    {
        $request = $this->container->get('utils.requests');
        $inputName = $request->get('name', 'request');
        $file_type = $request->get('type', 0);

        $result = ['errorCode' => 1, 'message' => ['message' => '', 'key' => $inputName, 'raw_value' => '']];

        if(!(isset($_FILES[$inputName]) && $_FILES[$inputName]['name'])){
            $result['message']['message'] = '上传失败,未获得资源';
            return $result;
        }

        if(!isset($this->file_types[$file_type])){
            $result['message']['message'] = '上传失败,类型不存在';
            return $result;
        }

        $path = implode(DIRECTORY_SEPARATOR, [$path, $this->getDefSavePath()]);

        $this->mkdirPath($path);

        $storage = new \Upload\Storage\FileSystem($path);
        $file = new \Upload\File($inputName, $storage);

        $filename = empty($filename) ? $this->getDefFileName() : $filename;
        $file->setName($filename);

        if(empty($file->getPathname())){
            $result['message']['message'] = '上传失败,'. $this->file_types[$file_type]['title'] . '过大';
            return $result;
        }

        //验证上传类型
        $file->addValidations(array(
            //验证文件类型  MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
            new \Upload\Validation\Mimetype($this->file_types[$file_type]['types']),
            //验证文件大小  use "B", "K", M", or "G"
            new \Upload\Validation\Size($this->file_types[$file_type]['size']),
        ));

        // Access data about the file that has been uploaded
        $data = array(
            'path_name' =>  $path . DIRECTORY_SEPARATOR . $file->getNameWithExtension(),
            'name'       => $file->getNameWithExtension(),
            'extension'  => $file->getExtension(),
            'mime'       => $file->getMimetype(),
            'size'       => $file->getSize(),
            'md5'        => $file->getMd5(),
            'dimensions' => $file->getDimensions(),
            'original_name' => $_FILES[$inputName]['name']
        );

        // Try to upload file
        try {
            // Success!
            $file->upload();
            $result['errorCode']  = 0;
            $result['data'] = $data;
            $result['message']['message'] = $this->file_types[$file_type]['title'] . '上传成功';
            $result['data']['server_path'] = $this->getFilePath($data['path_name']);

            //压缩图片
            if($file_type == 0){
                $width = $request->get('width', 0);
                $height = $request->get('height', 0);
                if($width != 0 && $height != 0){
                    Image::open(implode([Systems::rootdir(), 'web', $data['path_name']], DIRECTORY_SEPARATOR))
                        ->zoomCrop($width, $height)
                        ->save(implode([Systems::rootdir(), 'web', $data['path_name']], DIRECTORY_SEPARATOR));
                }
            }
        } catch (\Exception $e) {
            // Fail!
            $errorMessages = $file->getErrors();
            $result['message']['message'] = '上传失败'. $this->file_types[$file_type]['title'] . $errorMessages[0] ; //$errorMessages;
            $result['message']['raw_value'] = $file->getMimetype();
        }

        return $result;
    }

    /**
     * 删除文件 仅限upload内的文件
     *
     * @param $path
     */
    public function removeFile($path)
    {
        if(strstr($path, $this->container->getParameter('dir_upload')) === false) {
            return;
        }
        $path = dirname(dirname(dirname(__FILE__))) . '/web/' . $path;

        exec('rm -rf '. $path);
    }

    /**
     * 得到文件网络访问路径
     *
     * @param $file_path
     * @param string $extra_directory  以/结尾的子目录
     * @return string
     */
    public function getFilePath($file_path, $extra_directory = '/')
    {
        if($this->isFileLocal($file_path)) {
            if (!$this->validations->isEmpty($file_path)) {
                $file_path = $this->validations->filters->quotes($file_path);
                return $this->container->get('request')->getSchemeAndHttpHost() . $this->container->get('request')->getBasePath()  . $extra_directory . $file_path;
            }
        }else{
            if(!empty($file_path)){
                return $file_path;
            }
        }

        return '';
    }

    /**
     * 判断文件是否为系统上传
     *
     * @param $file_path
     * @return bool
     */
    public function isFileLocal($file_path)
    {
        if(strpos($file_path, $this->container->getParameter('dir_upload')) !== false) {
            return true;
        }

        return false;
    }

    /**
     * 拼接物理地址
     *
     * 用 / 连接目录  --方法舍弃
     *
     * @param array | string $path_param
     * @return string
     */
    public function physicalPathSplicing($path_param)
    {
        if(!is_array($path_param)){
            $path_param = array($path_param);
        }

        $path = '';

        foreach ($path_param as $value){
            $path .= $value .  DIRECTORY_SEPARATOR;
        }

        return rtrim($path, DIRECTORY_SEPARATOR) ;
    }


    /**
     * 下载保存文件
     *
     * @param $file_url_path
     * @return string
     */
    public function downloadSaveFile($file_url_path)
    {
        $save_path = $this->container->getParameter('dir_upload') . DIRECTORY_SEPARATOR . date('Y-m')."/";

        //创建目录
        if(!file_exists($save_path)) {
            $dirs = explode('/' , $save_path);
            $count = count($dirs);
            $path = '.';
            for ($i = 0; $i < $count; ++$i) {
                $path .= '/' . $dirs[$i];
                if(!is_dir($path)){//不是目录
                    if(!mkdir($path,0777)){
                    }
                }
            }
        }

        $save_path = $this->put_file_from_url_content($file_url_path, $name = uniqid().".jpg", $save_path);

        return $save_path;
    }

    /**
     * 写日志
     *
     * @param array|string $content  //可以为一维度数组
     * @param string $file_name
     */
    public function writeLog($content, $file_name = 'manages_dev.log')
    {
        if(!is_array($content)){
            $content = array($content);
        }

        foreach ($content as $index => $value){
            $content[$index] = sprintf('[%s]****%s****'. "\n", date('Y-m-d H:i:s'), $value);
        }

        file_put_contents($this->container->get('kernel')->getRootDir(). DIRECTORY_SEPARATOR .'logs'. DIRECTORY_SEPARATOR .$file_name , $content , FILE_APPEND);
    }

    /**
     * 读取日志
     *
     * @param string $file_name 日志名称
     * @param integer $rows 行数
     */
    public function readLog($file_name = 'manages_dev.log', $rows = 20)
    {
        $path = $this->container->get('kernel')->getRootDir(). DIRECTORY_SEPARATOR .'logs'. DIRECTORY_SEPARATOR. $file_name;
        if(!file_exists($path)){
            exit($file_name . '不存在');
        }
        $return = $this->read_file($path, $rows);
        for ($i = count($return) - 1; $i > -1; $i --){
            echo $return[$i];
            echo '<br>';
        }
    }

    /**
     * 读取文件最后几行
     *
     * @param $file
     * @param $lines
     * @return array
     */
    public function read_file($file, $lines) {
        //global $fsize;
        $handle = fopen($file, "r");
        $linecounter = $lines;
        $pos = -2;
        $beginning = false;
        $text = array();
        while ($linecounter > 0) {
            $t = " ";
            while ($t != "\n") {
                if(fseek($handle, $pos, SEEK_END) == -1) {
                    $beginning = true;
                    break;
                }
                $t = fgetc($handle);
                $pos --;
            }
            $linecounter --;
            if ($beginning) {
                rewind($handle);
            }
            $text[$lines-$linecounter-1] = fgets($handle);
            if ($beginning) break;
        }
        fclose ($handle);

        return array_reverse($text);
    }

    /**
     * 用过网络地址保存到本地
     *
     * @param $url
     * @param $saveName
     * @param $path
     * @return bool|string
     */
    function put_file_from_url_content($url, $saveName, $path)
    {
        // 设置运行时间为无限制
        set_time_limit ( 0 );

        $url = trim ( $url );
        $curl = curl_init ();
        // 设置你需要抓取的URL
        curl_setopt ( $curl, CURLOPT_URL, $url );
        // 设置header
        curl_setopt ( $curl, CURLOPT_HEADER, 0 );
        // 设置cURL 参数，要求结果保存到字符串中还是输出到屏幕上。
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 );
        // 运行cURL，请求网页
        $file = curl_exec ( $curl );
        // 关闭URL请求
        curl_close ( $curl );
        // 将文件写入获得的数据
        $filename = $path . $saveName;
        $write = @fopen ( $filename, "w" );
        if ($write == false) {
            return false;
        }
        if (fwrite ( $write, $file ) == false) {
            return false;
        }
        if (fclose ( $write ) == false) {
            return false;
        }

        return $filename;
    }
}
