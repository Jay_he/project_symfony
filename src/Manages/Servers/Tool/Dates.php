<?php
/**
 * 日期处理
 *
 */

namespace  Manages\Servers\Tool;

class Dates
{
    /**
     * 2个时间的截止时间
     *
     * @param $startDate
     * @param $endDate
     * @return null|string
     */
    function diffDate($startDate, $endDate)
    {
        $datetime1 = date_create($startDate);
        $datetime2 = date_create($endDate);
        $interval = date_diff($datetime1, $datetime2);

        $diff_date = null;

        if ($interval->y > 0) {
            $diff_date = $interval->format('%y年');
        } else if ($interval->m > 0) {
            $diff_date = $interval->format('%m月');
        } else if ($interval->d > 0) {
            $diff_date = $interval->format('%d天');
        } else if ($interval->h > 0) {
            $diff_date = $interval->format('%h小时');
        } else if ($interval->i > 0) {
            $diff_date = $interval->format('%i分钟');
        } else {
            $diff_date = $interval->format('%s秒');
        }

        return $diff_date;
    }

    /**
     * 计算月有多少天
     *
     * @param $month
     * @param string $year
     * @return int
     */
    public function getDays($month, $year = '')
    {
        $year = !$year ? date("Y") : $year;

        return cal_days_in_month(CAL_GREGORIAN, $month, $year);
    }

    /**
     * 获取年
     *
     * @return false|string
     */
    public function getYear()
    {
        return date('Y');
    }

    /**
     * 获取月
     *
     * @return false|string
     */
    public function getMonth()
    {
        return date('m');
    }

    /**
     * 获取时间
     *
     * @param string $format
     * @return false|string
     */
    public function getDateTime($format = 'Y-m-d H:i:s')
    {
        return date($format);
    }

}