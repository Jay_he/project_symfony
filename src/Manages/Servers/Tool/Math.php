<?php

/**
 * 数学计算
 */

namespace  Manages\Servers\Tool;


class Math
{
    /**
     * 保留小数点后几位,不进行四舍五入
     *
     * @param $val
     * @param int $precision
     * @return float
     */
    public function rountUp($val, $precision = 0)
    {
        $j = '1';
        for ($i = 0 ; $i < $precision; $i++){
            $j = (string)$j . (string)"0";
        }

        $r =  (float)$val * (float)$j;

        return floor((string)$r) / (float)$j;
    }


}
