<?php

/**
 * Session 改进
 */

namespace  Manages\Servers\Tool;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Sessions
{
    /**
     * session 对象
     *
     * @var object|\Symfony\Component\HttpFoundation\Session\Session
     */
    private $session;

    /**
     * session 前缀
     *
     * @var mixed
     */
    private $prefix;

    /**
     * 初始化
     *
     * Sessions constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->session = $container->get('session');
        $this->prefix = $this->container->getParameter('prefix_session');

    }

    /**
     * session 设置多条数据
     * 
     * @param mixed String|Array $data
     * @param String $value
     */
    public function set($data, $value = NULL)
    {        
        if (is_array($data)) {
            foreach ($data as $sessionName => $sessionValue) {
                $this->session->set($this->prefix . $sessionName, $sessionValue);
            }
        } else {
            $this->session->set($this->prefix . $data, $value);
        }
    }
    
    /**
     * 判断session是否存在
     * 
     * @param String $sessionName
     * @return boolean
     */
    public function has($sessionName)
    {
        return $this->session->has($this->prefix . $sessionName);
    }
    
    /**
     * 获取session值
     *
     * @param $sessionName
     * @return mixed
     */
    public function get($sessionName)
    {
        return $this->session->get($this->prefix . $sessionName);
    }
    
    /**
     * 删除session值
     * 
     * @param $sessionName
     */
    public function remove($sessionName)
    {
        $this->session->remove($this->prefix . $sessionName);
    }
    
    /**
     *  清楚session
     */
    public function clear()
    {
        $this->session->clear();
    }

    /**
     * 得到名字
     *
     * @param $sessionName
     * @return string
     */
    public function getName($sessionName)
    {
        return $this->prefix . $sessionName;
    }
}
