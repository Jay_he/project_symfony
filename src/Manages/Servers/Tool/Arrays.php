<?php

/**
 * 数组处理
 */

namespace  Manages\Servers\Tool;

class Arrays
{

    /**
     * 得到数组中的内容 没有则返回设置的默认值
     *
     * @param array $array
     * @param $key
     * @param string $default
     * @return mixed|null|string
     */
    public function value(array $array, $key, $default = null)
    {
        // isset() is a micro-optimization - it is fast but fails for null values.
        if(isset($array[$key])) {
            return $array[$key];
        }
        // Comparing $default is also a micro-optimization.
        if($default === null || array_key_exists($key, $array)) {
            return null;
        }

        return $default;
    }

    /**
     * 数据去重累加
     *
     * @param $array
     * @param string $reference_str  参照str
     * @param string $accumulation_str  累加str
     * @return mixed
     */
    public function heavyAccumulation($array , $reference_str, $accumulation_str)
    {
        $unset = array();

        for ($i = 0; $i < count($array); $i++){
            for ($j = $i+1; $j < count($array); $j++){
                if($array[$i][$reference_str] == $array[$j][$reference_str] && !in_array($j, $unset)){
                    $array[$i][$accumulation_str] += $array[$i][$accumulation_str];
                    $unset[] = $j;
                }
            }
        }

        foreach ($unset as $index => $value){
            unset($array[$value]);
        }

        return array_merge($array);
    }

    /**
     * 判断一维数组是否重复
     *
     * @param $array
     * @return bool
     */
    public function hasDuplicate($array)
    {
        $dupe_array = array();
        foreach ($array as $value) {
            if ( isset($dupe_array[$value]) ) {
                return true;
            } else {
                $dupe_array[$value] = true;
            }
        }

        return false;
    }

    /**
     * 二维数组是否重复
     *
     * @param $array
     * @param string $key 判断参照的数组kay
     * @return bool
     */
    public function hasTwoDimensionDuplicate($array, $key)
    {
        $dupe_array = array();
        foreach ($array as $value) {
            if ( !isset($value[$key]) ) {
                return false;
            }
            if ( isset($dupe_array[$value[$key]]) ) {
                return true;
            } else {
                $dupe_array[$value[$key]] = true;
            }
        }

        return false;
    }

    /**
     * 数据层级化
     *
     * @param array $arr
     * @param string $id
     * @param string $pid
     * @return mixed
     */
    public function findChildren(array $arr, $id = 'id', $pid = 'parent_id')
    {
        $data = [];
        foreach($arr as $v) $data[$v[$id]] = $v;
        foreach ($data as $k => $item){
            if( $item[$pid]) {
                $data[$item[$pid]]['child'][$item[$id]] = & $data[$k];
            }
        }

        foreach ($data as $key=>$value) {
            if ($value['parent_id']) {
                unset($data[$key]);
            }
        }

        return $this->fixKeys($data);
    }

    /**
     * 整理key
     *
     * 功能实现策略：如果数组key中存在一个数字就将该数组中的全部value取出变为新的数组
     *
     * @param $array
     * @return array
     */
    public function fixKeys($array)
    {
        $numberCheck = false;
        foreach ($array as $k => $val) {
            if (is_array($val)) {
                $array[$k] = $this->fixKeys($val); //递归
                if (is_numeric($k)) $numberCheck = TRUE;
            }
        }

        if ($numberCheck === true) {
            return array_values($array);
        } else {
            return $array;
        }
    }

    /**
     * 数组有序化
     *
     * 用于输出到客户端自定义数组的排序封装使用
     *
     * @param array $array
     * @return array
     */
    public function ordering($array)
    {
        $ordering_array = array();
        foreach ($array as $key => $value){
            $ordering_array[] = array(
                'key' => $key,
                'value' => $value
            );
        }

        return $ordering_array;
    }
}
