<?php

/**
 * 验证类 + 报错信息记录
 */

namespace  Manages\Servers\Tool;

use Manages\App\ConstReg;

class Validations
{
    /**
     * 错误信息储存
     *
     * @var array
     */
    private static $error = [];

    /**
     * 单个用户信息储存 [用完即删除-用于锁定错误的产生处]
     *
     * @var null
     */
    private $key = null;

    /**
     * 过滤服务
     *
     * @var Filters
     */
    public $filters;

    /**
     * 初始化
     *
     * Validations constructor.
     * @param Filters $filters
     */
    public function __construct(Filters $filters)
    {
        $this->filters = $filters;
    }


    public function setKey($key)
    {
        $this->key = $key;
    }

    private function getKey()
    {
        $key = $this->key;
        if ($key !== null) {
            $this->destroyKay();
        }
        return $key;

    }

    private function destroyKay()
    {
        $this->key = null;
    }


    /**
     * 返回第一条error信息
     *
     * @return null
     */
    public function getError()
    {
        return reset(self::$error);
    }

    /**
     * 设置一条error信息
     *
     * @param $message
     * @param null|array $raw_value 原始值|报错值
     * @param $key
     */
    public function setError($message, $raw_value = null, $key = '')
    {
        $message = array('message' => $message);
        $message['raw_value'] = $raw_value;

        if (!empty($key)) {
            $this->setKey($key);
        }

        $message['key'] = $this->getKey();
        if (empty($message['key'])) {
            self::$error[] = $message;
        } else {
            self::$error[$message['key']] = $message;
        }
    }

    /**
     * 返回所有error信息
     *
     * @return null
     */
    public function getErrors()
    {
        return self::$error;
    }

    /**
     * 字符串基础验证方法
     *
     * @param $str
     * @param $name
     * @param bool $nullable
     * @param string $reg
     * @param int $min
     * @param int $max
     * @return bool
     */
    public function regexp($str, $name, $nullable = false, $reg = '', $min = 0, $max = 0)
    {
        if (!$nullable && $this->isEmptyAll($str)) {
            $this->setError('请输入' . $name, $str);
            return false;
        }

        if (!empty($str) && $this->isEmpty($str)) {
            $this->setError($name . '不能只包含空格', $str);
            return false;
        }

        if ($reg) {
            if (!$this->isEmpty($str)) {
                if (!preg_match($reg, $str)) {
                    $this->setError($name . '格式不正确', $str);
                    return false;
                }
            }
        }

        if (!$this->isEmpty($str)) {
            if (($min > 0 || $max > 0) && (mb_strlen($str, 'UTF-8') < $min || mb_strlen($str, 'UTF-8') > $max)) {
                $this->setError(($min == 0) ? $name . '长度不能超过' . $max . '位' : $name . '长度在' . $min . '-' . $max . '位', $str);
                return false;
            }
        }

        $this->destroyKay();
        return true;
    }


    /**
     * 判断字符中是否为空真实为空 --逻辑空
     *
     * 1:只有空格换行的值为空
     *
     * @param $str
     * @return bool
     */
    public function isEmpty($str)
    {
        return (empty($this->filters->space($str)) && $str !== 0 && $str !== '0') ? true : false;
    }

    /**
     * 判断字符中是否为空真实为空 --逻辑空
     *
     * 1：只有空格换行的值为空
     * 2：0 和 ‘0’ 不是空
     *
     * @param $str
     * @return bool
     */
    public function isEmptyAll($str)
    {
        return (empty($this->filters->space($str)) && $str !== 0 && $str !== '0') ? true : false;
    }

    /**
     * 验证字符串
     *
     * @param $str
     * @param $name
     * @param bool $nullable
     * @param int $max
     * @return bool
     */
    public function isString($str, $name, $nullable = false, $max = 255)
    {
        return $this->regexp($str, $name, $nullable, '', 0, $max);
    }

    /**
     * 验证是否为身份证号码
     *
     * @param $str
     * @param string $name
     * @param bool $nullable
     * @return bool
     */
    public function isIdNumber($str, $name = '身份证号码', $nullable = false)
    {
        return $this->regexp($str, $name, $nullable, ConstReg::REG_ID_CARD_NO);
    }

    /**
     * 判断是否是名称
     *
     * @param $str
     * @param string $name
     * @param bool $nullable
     * @param int $min
     * @param int $max
     * @return bool
     */
    public function isName($str, $name = '名称', $nullable = false, $min = 2, $max = 15)
    {
        return $this->regexp($str, $name, $nullable, '', $min, $max);
    }

    /**
     * 验证是否邮箱
     *
     * @param $str
     * @param string $name
     * @param bool $nullable
     * @return bool
     */
    public function isEmail($str, $name = '邮箱', $nullable = false)
    {
        return $this->regexp($str, $name, $nullable, ConstReg::REG_EMAIL, 0, 50);
    }

    /**
     * 验证是否手机号码
     *
     * @param $str
     * @param string $name
     * @param bool $nullable
     * @return bool
     */
    public function isMobile($str, $name = '手机号', $nullable = false)
    {
        return $this->regexp($str, $name, $nullable, ConstReg::REG_MOBILE);
    }

    /**
     * 判断座机
     *
     * @param $str
     * @param string $name
     * @param bool $nullable
     * @return bool
     */
    public function isTelephone($str, $name = '座机号码', $nullable = false)
    {
        return $this->regexp($str, $name, $nullable, ConstReg::REG_TELEPHONE);
    }

    /**
     * 验证密码
     *
     * @param $str
     * @param string $name
     * @param bool $nullable
     * @param int $min
     * @param int $max
     * @return bool
     */
    public function isPassword($str, $name = '密码', $nullable = false, $min = 6, $max = 16)
    {
        return $this->regexp($str, $name, $nullable, ConstReg::REG_PASSWORD, $min, $max);
    }

    /**
     * 是否是含有2位小数的价格
     *
     * @param $str
     * @param string $name
     * @param bool $nullable
     * @param int $min
     * @param int $max
     * @return bool
     */
    public function isPrice($str, $name = '金额', $nullable = false)
    {
        return $this->regexp($str, $name, $nullable, ConstReg::REG_PRICE);
    }

    /**
     * 判断时候是否QQ号码
     *
     * @param $str
     * @param string $name
     * @param bool $nullable
     * @return bool
     */
    public function isQQ($str, $name = 'QQ号', $nullable = false)
    {
        return $this->regexp($str, $name, $nullable, ConstReg::REG_QQ);
    }

    /**
     * 判断经度
     *
     * @param $str
     * @param string $name
     * @param bool $nullable
     * @return bool
     */
    public function isLng($str, $name = '经度', $nullable = false)
    {
        return $this->regexp($str, $name, $nullable, ConstReg::REG_LNG);
    }


    /**
     * 判断整数
     *
     * @param $number
     * @param int $min
     * @param int $max
     * @return mixed
     */
    public function isInt($number, $min = 0, $max = 999999)
    {
        $int_options = array(
            "options" => array(
                "min_range" => $min,
                "max_range" => $max
            )
        );

        return filter_var($number, FILTER_VALIDATE_INT, $int_options) !== false;
    }

    /**
     * 判断小数
     *
     * @param $number
     * @param int $min
     * @param int $max
     * @return bool
     */
    public function isFloat($number, $min = 0, $max = 999999)
    {
        $int_options = array(
            "options" => array(
                "min_range" => $min,
                "max_range" => $max
            )
        );

        return filter_var($number, FILTER_VALIDATE_FLOAT, $int_options) !== false;
    }

    /**
     * 判断经度
     *
     * @param $str
     * @param string $name
     * @param bool $nullable
     * @return bool
     */
    public function isLat($str, $name = '纬度', $nullable = false)
    {
        return $this->regexp($str, $name, $nullable, ConstReg::REG_LAT);
    }

    /**
     * 判断日期
     *
     * @param $str
     * @param string $date_format Y-m-d H:i:s
     * @return bool
     */
    public function isDate($str, $date_format = 'Y-m-d')
    {
        $unixTime = strtotime($str);
        if (!$unixTime) {
            return false;
        }

        if (date($unixTime, $date_format) != $str) {
            return false;
        }

        return true;
    }

    /**
     * 判断是否是闰年
     *
     * @param $year
     * @return bool
     */
    public function isLeapYear($year)
    {
        return ((($year % 4) == 0) && ((($year % 100) != 0) || (($year % 400) == 0)));
    }

    /**
     * 判断网址
     *
     * @param $url
     * @return bool
     */
    public function isUrl($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL) !== false;
    }

    /**
     * 判断ip
     *
     * @param $ip
     * @return bool
     */
    public function isIp($ip)
    {
        return filter_var($ip, FILTER_VALIDATE_IP) !== false;
    }

    /**
     * 检查图片大小
     *
     * @param $resources
     * @param $width
     * @param $height
     * @return bool
     */
    public function checkImageSize($resources, $width = 360, $height = 180)
    {
        if(!isset($_FILES[$resources])){
            $this->setError('未获取资源');
            return false;
        }

        $resources = $_FILES[$resources];

        $temp = $resources['tmp_name'];
        $img_info = getimagesize($temp);

        if($img_info['0'] != $width || $img_info['1'] != $height){
           $this->setError('请上传'. $width . '*'. $height . '尺寸图片');
            return false;
        }

        return true;
    }
}
