<?php

/**
 * 图片处理
 */

namespace  Manages\Servers\Tool;

use Endroid\QrCode\QrCode;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

class Images
{
    /**
     * container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * 初始化
     *
     * Images constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * 生成头像--白底黑字
     *
     * @param int $width 画布宽
     * @param int $height 画布高
     * @param string $text 文字
     * @param string $picture_name 图片名称
     * @param int $x 字体距离左的位置
     * @param int $y 字体距离右的位置
     * @param int $font_size 字体大小
     * @param string $font 字体类型
     */
    function generateAvatar($width = 60, $height = 60, $text = 'Hello World', $picture_name = '', $x = 5 , $y = 10,  $font_size = 12, $font = "../web/assets/fonts/simsun.ttc")
    {
        $myImage = ImageCreate($width, $height); //参数为宽度和高度
        ImageColorAllocate($myImage, 255, 255, 255);//第一次背景上色
        //第二次字体上色
        //$white = ImageColorAllocate($myImage, 255, 255, 255);
        $black = ImageColorAllocate($myImage, 0, 0, 0);
        // $red=ImageColorAllocate($myImage, 255, 0, 0);
        // $green=ImageColorAllocate($myImage, 0, 255, 0);
        // $blue=ImageColorAllocate($myImage, 0, 0, 255);
        $angle = 0;
        imagettftext($myImage, $font_size, $angle, $x, $y, $black, $font, $text);
        imagepng($myImage, __DIR__ . "/../../web/upload/" . $picture_name);
    }

    /**
     * 二维码
     *
     * @param string $text  内容（网址）
     * @param int $size  大小
     * @param string $title 标题
     */
    public function qrcode($text = 'http://www.baidu.com', $size = 300, $title = '')
    {
        $qrCode = new QrCode();
        $qrCode
            ->setText($text)
            ->setSize($size)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
            ->setLabel($title)
            ->setLabelFontSize(16)
            ->setImageType(QrCode::IMAGE_TYPE_PNG)
        ;

        // now we can directly output the qrcode
        header('Content-Type: '.$qrCode->getContentType());
        $qrCode->render();
    }

}
