<?php
/**
 * 主控类[此类不允许编辑--框架更新的时候会将编辑信息覆盖]
 * 
 * 集成symfony的基本库--进行代码的主流程控制
 *
 * [注意]
 * 
 * User: zmit
 * Date: 6/30/17
 * Time: 9:26 PM
 */

namespace Manages;

use Doctrine\DBAL\Connection;
use Manages\Model\AbstractModel;
use Manages\Servers\InputOutPut\Requests;
use Manages\Servers\InputOutPut\Responses;
use Manages\Servers\Tool\Validations;
use Manages\Solution\Admin\Admin;
use Manages\Solution\Doc\Sql;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MastersController extends Controller
{
    /**
     * 初始化
     *
     * MastersController constructor.
     */
    public function __construct(ContainerInterface $container = null)
    {
        header("Content-Type: text/html; charset=utf-8");
        $this->setContainer($container);
    }


    /**
     * 来源
     * 
     * @var
     */
    private static $platform;

    /**
     * 登录用户id
     *
     * @var
     */
    private static $user_id;

    /**
     * 返回类型
     *
     * @var
     */
    private static $return_type;

    /**
     * 版本号
     *
     * @var
     */
    private static $version;

    /**
     * 用户登录标识
     *
     * @var
     */
    private static $token;


    /**
     * 全局返回值--在返回值中拼接
     *
     * @var
     */
    public static $rdata;


    //TODO 常用服务

    /**
     * @var Connection
     */
    protected static $conn = null;
    /**
     * @var Sql
     */
    protected static $s_sql  = null;
    /**
     * @var Validations
     */
    protected static $s_verify = null;

    /**
     * @var Requests
     */
    protected static $s_request = null;

    /**
     * @var Admin
     */
    protected static $s_admin = null;

    /**
     * 得到platform
     *
     * @return mixed
     */
    public function getPlatform()
    {
        return self::$platform;
    }

    /**
     * 设置platform
     *
     * @param mixed $platform
     */
    public function setPlatform($platform)
    {
        self::$platform = $platform;
    }

    /**
     * 设置user_id
     *
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        self::$user_id = $user_id;
    }

    /**
     * 得到user_id
     *
     * @return mixed
     */
    public function getUserId()
    {
        return self::$user_id;
    }

    /**
     * 设置return_type
     *
     * @param $return_type
     */
    public function setReturnType($return_type)
    {
        self::$return_type = $return_type;
    }

    /**
     * 得到return_type
     *
     * @return mixed
     */
    public function getReturnType()
    {
        return self::$return_type;
    }

    /**
     * 得到版本号
     *
     * @return mixed
     */
    public function getVersion()
    {
        return self::$version;
    }

    /**
     * 设置版本号
     *
     * @param $version
     */
    public function setVersion($version)
    {
        self::$version = $version;
    }

    /**
     * 得到token
     *
     * @return mixed
     */
    public function getToken()
    {
        return self::$token;
    }

    /**
     * 设置token
     *
     * @param $token
     */
    public function setToken($token)
    {
        self::$token = $token;
    }

    /**
     * 入口方法
     *
     * @param string $return_type  | return_type_ajax| return_type_twig
     * @param bool $is_login  是否必须登录  true  false
     * @return bool | object
     */
    public function inlet($return_type = null , $is_login = true)
    {
        if(!empty($return_type)){
            $this->setReturnType($return_type);
        }

        return true;
    }

    /**
     * 调用其他bundle数据
     *
     * @param $BundleControllerAction
     * @param array $path
     * @param string $return_type  返回类型默认是twig | ajax
     * @return mixed
     */
    public function getApiData($BundleControllerAction, array $path = array(), $return_type = 'twig')
    {
        $bundle_info = $this->forward($BundleControllerAction, $path)->getContent();

        $data = json_decode($bundle_info, TRUE);

        if (!is_array($data)) {
            exit($bundle_info);
        }

        if ($return_type == $this->getParameter('return_type_twig')) {
            if ($data['errorCode'] == $this->getParameter('error_code_not_logged')) {
                $this->goUrl($data['data']['go_url']);
            }

            if ($data['errorCode'] == $this->getParameter('error_code_failure')) {
                $this->ex($data['message']);
            }
        }

        return $data;
    }

    /**
     * 跳转url
     *
     * @param $url
     */
    public function goUrl($url)
    {
        Header("Location:{$url}");exit;
    }

    /**
     * 得到URL
     *
     * @param string $route  路径名
     * @param array $parameters  路径参数
     * @return string
     */
    public function getUrl($route, $parameters = array())
    {
        return $this->get('utils.requests')->getHost()  . $this->generateUrl($route, $parameters);
    }

    /**
     * Url拼接固定参数
     *
     * @param $url
     * @return mixed
     */
    public function urlFixationParameter($url)
    {
        $url_parameter = array();

        if(!empty($this->getPlatform())){
            $url_parameter[$this->getParameter('param_platform')] = $this->getPlatform();
        }
        if(!empty($this->getVersion())){
            $url_parameter[$this->getParameter('param_version')] = $this->getVersion();
        }
        if(!empty($this->getToken())){
           $url_parameter[$this->getParameter('param_token')] = $this->getToken();
        }

        $pre = strpos($url, '?') === false ? '?' : '&';
        $i = 0;

        foreach ($url_parameter as $key => $value){
            $url .= $i != 0 ? '&': $pre . $key . '=' . $value;
            $i++;
        }

        return $url;
    }

    /**
     * 返回值处理Json
     *
     * @param  $message
     * @param int $errorCode
     * @param array $data
     * @return array|\Symfony\Component\HttpFoundation\Response|bool
     */
    protected function response($message  , $errorCode = 1, $data = array())
    {
        if($this->getReturnType() == $this->getParameter('return_type_ajax')) {
            return  Responses::json($message, $errorCode, $data);
        }else{
            $this->ex($message);
        }

        return false;
    }

    /**
     * 访问异常
     *
     * @param string | array $message
     * @param string $url
     */
    public function ex($message = '访问异常!', $url = 'javascript:history.go(-1);')
    {
        if(is_array($message)){
            $message = $message['message'];
        }

        if($this->getPlatform() == $this->getParameter('from_admin')){
            exit($this->render(':error:admin_error.html.twig', array('message' => $message, 'url' => $url)));
        }else{
            exit($this->render(':error:business_error.html.twig', array('message' => $message, 'url' => $url)));
        }
    }

    /**
     * 设置list数据
     *
     * @param $array
     * @param AbstractModel $model  model实体类
     * @return mixed
     */
    public function setList($array , $model)
    {
        foreach ($array as $index => $value){
            $array[$index] = $model->setInfo($value);
        }

        return $array;
    }

    /**
     * 设置项目数据[项目输出的总控制]
     *
     * @param $info
     * @return array
     */
    public function setProjectInfo($info)
    {
        if (empty($info)){
            return array();
        }

        return $info;
    }

    /**
     * 写日志
     *
     * @param array|string $content  //可以为一维度数组
     * @param string $file_name
     */
    public function writeLog($content, $file_name = 'manages_dev.log')
    {
        $this->get('services.files')->writeLog($content, $file_name);
    }
}