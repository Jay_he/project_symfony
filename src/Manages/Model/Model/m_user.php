<?php

/**
 * 用户表
 * User: Jay
 * DateTime: 7/17/1712:52 AM
 */

namespace Manages\Model\Model;

use Manages\Model\AbstractModel;
use Manages\Servers\InputOutPut\Responses;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Acl\Exception\Exception;

class m_user extends AbstractModel
{
    function __construct(ContainerInterface $container, $table_name = null)
    {
        $table_name = 'user';
        parent::__construct($container, $table_name);
    }

    function insertOrUpdate($id = 0)
    {
        // TODO: Implement insertOrUpdate() method.
        $data = self::$s_request->getPostAll();

        try {
            if ($id == 0) {
                self::$conn->insert('user', $data);
                return Responses::arrays('添加成功', 0);
            } else {
                self::$conn->update('user', $data, array('id' => $id));
                return Responses::arrays('编辑成功', 0);

            }
        }catch (Exception $exception){
            return Responses::arrays($exception, 1);
        }

    }

    public function rewritesRule($rule)
    {
        if(array_key_exists('key', $rule) && array_key_exists('val', $rule)) {
            if ($rule['key'] != '' && $rule['val'] != '') {
                $rule['where'][] = " AND {$rule['key']} LIKE '%{$rule['val']}%'";
            }
        }

        return $rule;
    }
}