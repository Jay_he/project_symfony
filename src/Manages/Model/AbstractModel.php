<?php
/**
 * 实体类父类
 *
 * Jay
 *
 * 子类可对其方法在功能不变的情况下进行重写
 */
namespace Manages\Model;

use Manages\ProjectController;
use Manages\Standard\Model\InterfaceModel;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractModel extends ProjectController implements InterfaceModel
{
    const OUTPUT_MODE_ALL = 0;  //输出模式   全部模式数据
    const OUTPUT_MODE_COUNT = 1;  //输出模式  返回查询总数[不分页]
    const OUTPUT_MODE_ALL_LIST = 2;  //输出模式  返回查询全部的数据
    const OUTPUT_MODE_LIMIT_LIST = 3;  //输出模式 返回分页全部的数据
    const OUTPUT_MODE_SQL = 4;  //输出模式 返回查询的SQL语句
    const OUTPUT_MODE_SQL_ARRAY = 5; //输出模式 返回查询SQL语句的组成部分数组

    const RULE_SELECT = 'select';  //默认支持规则select  自定义查询字段 参数格式 string
    const RULE_ORDER_BY = 'order_by';  //默认支持规则order_by  自定义优先排序方式 （支持一维数组和字符串） 单个数组中为字符串  条件会自动拼接到sql中 参数开头必须是 ASC DESC
    const RULE_WHERE = 'where'; //默认支持规则where 自定义查询条件 数组（支持一维数组和字符串） 单个数组中为字符串  条件会自动拼接到sql中 参数开头必须是 AND 或者 OR

    protected $sql = '';

    protected $table_name = null;

    protected $sql_array = array(
        'pre' => '',    //主表前缀
        'select' => '',
        'from' => '',
        'join' => '',
        'where' => '',
        'order_by' => '',
        'final_order_by' => '.id DESC ', //最终排序
        'primary_key' => 'id'  //用于getInfo查询使用
    );

    protected $tel_sql_array = array(
        'pre' => '',    //主表前缀
        'select' => '',
        'from' => '',
        'join' => '',
        'where' => '',
        'order_by' => '',
        'final_order_by' => '.id DESC ', //最终排序
        'primary_key' => 'id'  //用于getInfo查询使用
    );

    /**
     * Model constructor.
     *
     * @param ContainerInterface $container
     * @param null $table_name
     */
    public function __construct(ContainerInterface $container, $table_name = null)
    {
        $this->container = $container;
        $this->table_name = $table_name;
    }

    /*
     * 规则重写再重写 用户对rewriteRule进行部分重写
     */
    public function rewritesRule($rule)
    {
        return $rule;
    }

    /**
     * 重写规则  建议重写
     *
     * @param $rule
     */
    public function rewriteRule($rule)
    {
        $this->sql_array['pre'] = 't';
        $this->sql_array['from'] = $this->table_name . ' as '. $this->sql_array['pre'];
        if (!array_key_exists('select', $rule)) {
            $this->sql_array['select'] = self::$s_sql->getTableColumnName($this->table_name, $this->sql_array['pre'] . '.');
        }

        return $this->rewritesRule($rule);
    }

    /**
     * 重写INFO规则 为setInfo()的嵌套重写方法
     *
     * @param $info
     * @return mixed
     */
    public function rewriteInfo($info)
    {
        return $info;
    }
    /**
     * 重写删除方法 为delete()的嵌套重写再重写方法
     *
     * @param $id
     */
    public function rewritesDelete($id){}

    /**
     * 重写删除方法 为delete()的嵌套重写方法
     *
     * @param $id
     * @return int
     */
    public function rewriteDelete($id)
    {
        $this->rewritesDelete($id);

        return self::$conn->executeUpdate('DELETE FROM ' . $this->table_name . ' WHERE id in ('. $id . ')');
    }

    /**
     * [抽象方法]编辑和新增方法 子类必须重写
     *
     * @param int $id
     * @param array $data
     * @return mixed
     */
    abstract function insertOrUpdate($id = 0, $data = []);

    /*
     * 规则
     *
     * @param array $rule
     * @param string $id
     * @return null
     */
    public function rule($rule = array(), $id = '')
    {
        $this->sql_array = $this->tel_sql_array;
        
        // TODO: Implement rule() method.
        $rule = $this->rewriteRule($rule);

        if(isset($rule['select'])){
            $this->sql_array['select'] = $rule['select'] . $this->sql_array['select'];
        }

        if(isset($rule['order_by'])){
            if (is_array($rule['order_by'])){
                foreach ($rule['order_by'] as $key => $value) {
                    $this->sql_array['order_by'] .= '' .$value  . ' ,';
                }
            }else{
                $this->sql_array['order_by'] .= '' . $rule['order_by'].  ' ,';
            }
        }


        if(isset($rule['where'])){
            if(is_array($rule['where'])){
                foreach ($rule['where'] as $key => $value) {
                    $this->sql_array['where'] .= '' .$value . ' ';
                }
            }else{
                $this->sql_array['where'] .= '' . $rule['where']. ' ';
            }
        }

        if($id !== ''){
            $this->sql_array['where'] .= ' AND ' . $this->sql_array['pre'] .'.'. $this->sql_array['primary_key'] . ' = ' . "'$id'";
        }

        $this->sql_array['order_by'] .= ' '. $this->sql_array['pre'] . $this->sql_array['final_order_by'];

        $this->generateSql();
    }



    /**
     * 设置info输出
     *
     * @param $info
     * @return array
     */
    public function setInfo($info)
    {
        return empty($info) ? array() : $this->setProjectInfo($this->rewriteInfo($info));
    }

    /**
     * 删除
     *
     * @param $id
     * @return bool|int
     */
    public function delete($id)
    {
        return empty($id) ? false : $this->rewriteDelete($id);
    }

    /**
     * 得到列表
     *
     * @param array $rule 规则
     * @param array $output_mode 输出模式
     * @return array
     */
    public function getList(array $rule = array(), $output_mode = array(self::OUTPUT_MODE_LIMIT_LIST, self::OUTPUT_MODE_COUNT))
    {
        // TODO: Implement getList() method.
        $rule['function'] = 'list';
        $this->rule($rule);
        if(!is_array($output_mode)){
            $output_mode = array($output_mode);
        }
        $return_array = [];
        foreach ($output_mode as $value){
            if($value == self::OUTPUT_MODE_ALL || $value == self::OUTPUT_MODE_COUNT){
                $return_array['count'] = self::$s_sql->getCount($this->getSql());
            }
            if($value == self::OUTPUT_MODE_ALL || $value == self::OUTPUT_MODE_ALL_LIST){
                $return_array['info'] = $this->setList(self::$conn->fetchAll($this->getSql()), $this);
            }
            if($value == self::OUTPUT_MODE_ALL || $value == self::OUTPUT_MODE_LIMIT_LIST){
                $return_array['info'] = $this->setList(self::$conn->fetchAll(self::$s_sql->sqlMosaicLimit($this->getSql())), $this);
            }
            if($value == self::OUTPUT_MODE_ALL || $value == self::OUTPUT_MODE_SQL){
                $return_array['sql'] = $this->getSql();
            }
            if($value == self::OUTPUT_MODE_ALL || $value == self::OUTPUT_MODE_SQL_ARRAY){
                $return_array['sql_array'] = $this->getSqlArray();
            }
        }

        return $return_array;
    }

    /**
     * 得到详情
     *
     * @param $id  '' 此参数无效 不参与查询
     * @param array $rule 规则
     * @return array|mixed
     */
    public function getInfo($id, array $rule = array())
    {
        // TODO: Implement getInfo() method.
        $rule['function'] = 'info';
        $this->rule($rule, $id);
        return $this->setInfo(self::$conn->fetchAssoc($this->sql . ' LIMIT 1'));
    }

    /**
     * 得到一个值
     *
     * @param $column
     * @param array $rule
     * @param bool $def
     * @return bool|mixed
     */
    public function getColumn($column, $rule = array(), $def = false)
    {
        $rule['function'] = 'info';
        $this->rule($rule);
        $this->sql_array['select'] = $column;
        $this->generateSql();
        return self::$conn->fetchColumn($this->sql . ' LIMIT 1');
    }

    /**
     * 生成sql
     *
     * @param null $sql
     */
    public function generateSql($sql = null)
    {
        if(empty($sql)) {
            $sql = 'SELECT ' . $this->sql_array['select'] . ' FROM ' . $this->sql_array['from'] . ' ' . $this->sql_array['join'] . ' WHERE 1 ' . $this->sql_array['where'] . '  ORDER BY ' . $this->sql_array['order_by'];
        }

        $this->sql = str_replace('sql_pre', $this->sql_array['pre'] ,$sql);
    }

    public function getSql()
    {
        return $this->sql;
    }

    public function getSqlArray()
    {
        return $this->sql_array;
    }

    public function getTableName()
    {
        return $this->table_name;
    }
    
    public function getPre($pre = null)
    {
        return (empty($pre) ? $this->sql_array['pre'] : $pre ) . '.';
    }
}