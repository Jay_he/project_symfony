<?php

/**
 * 项目根地址
 */
define('PATH', dirname(dirname(dirname(dirname(__DIR__)))));

/**
 * 输出text
 *
 * @param $text
 */
function echo_text($text = ''){
    echo $text;
    echo "\n";
}

/**
 * 读取配置文件
 *
 * @param $configure_file_path
 * @return array
 */
function read_configure($configure_file_path , $path = PATH)
{
    $dir_array = array();

    $file = file($configure_file_path, FILE_IGNORE_NEW_LINES);
    for($i = 0; $i < count($file) ; $i++){
        $str = $file[$i];
        if(!empty($str)) {
            $tag  = substr($str, 0, 1);
            switch ($tag){
                case '!':
                    $str = substr($str, 1, strlen($str) - 1);
                    unset($dir_array[$path. $str]);
                    break;
                case '#':
                        continue;
                    break;
                case '&':
                    $str = substr($str, 1, strlen($str) - 1);
                    $dir_array[$path . $str ] = 1;
                    break;
                default:
                    //读取当前目录下的全部文件名
                    $dir_array = array_merge($dir_array, getDirArray($path . $str));

            }
        }
    }

    return $dir_array;
}

/**
 * 删除一个目录下全部的空目录
 *
 * @param $path
 */
function rm_empty_dir($path){
    if(is_dir($path) && ($handle = opendir($path))!==false){
        while(($file=readdir($handle))!==false){     // 遍历文件夹
            if($file!='.' && $file!='..'){
                $curfile = $path.'/'.$file;          // 当前目录
                if(is_dir($curfile)){                // 目录
                    rm_empty_dir($curfile);          // 如果是目录则继续遍历
                    if(count(scandir($curfile))==2){ // 目录为空,=2是因为. 和 ..存在
                        rmdir($curfile);             // 删除空目录
                    }
                }
            }
        }
        closedir($handle);
    }
}


/**
 * 将目录下的文件目录变为数组
 *
 * @param $dir_name
 * @return array
 */
function getDirArray($dir_name)
{
    $arr = @scandir($dir_name);
    $return_array = [];
    if(!empty($arr)) {
        unset($arr[0]);
        unset($arr[1]);
        foreach ($arr as $value) {
            $return_array[$dir_name . '/' . $value] = 1;
        }
    }
    return $return_array;
}

/**
 * 删除内容所在的行
 *
 * @param $filePath
 * @param $target
 */
function delTargetLine($filePath, $target)
{
    $result = null;
    $fileCont = file_get_contents($filePath);
    $targetIndex = strpos($fileCont, $target); #查找目标字符串的坐标

    if ($targetIndex !== false) {
        #找到target的前一个换行符
        $preChLineIndex = strrpos(substr($fileCont, 0, $targetIndex + 1), "\n");
        #找到target的后一个换行符
        $AfterChLineIndex = strpos(substr($fileCont, $targetIndex), "\n") + $targetIndex;
        if ($preChLineIndex !== false && $AfterChLineIndex !== false) {
            #重新写入删掉指定行后的内容
            $result = substr($fileCont, 0, $preChLineIndex + 1) . substr($fileCont, $AfterChLineIndex + 1);
            file_put_contents($filePath, $result);
            //$fp = fopen($filePath, "w+");
            //fwrite($fp, $result);
            //fclose($fp);
        }
    }
}