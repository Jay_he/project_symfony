<?php

include_once dirname(dirname(__FILE__)) . '/bin/tool.php';

$del_array = read_configure(PATH. '/src/Manages/Bin/install/.install');

foreach ($del_array as $index => $value){
    echo_text("rm -rf " . $index);
    echo exec("rm -rf " . $index);
}

rm_empty_dir(PATH);

delTargetLine(PATH . '/app/config/config.yml', '- { resource: "@ManagesBundle/Resources/config/services.yml" }');
delTargetLine(PATH . '/app/AppKernel.php', 'new ManagesBundle\ManagesBundle(),');
delTargetLine(PATH . '/app/config/routing.yml', 'manages:');
delTargetLine(PATH . '/app/config/routing.yml', 'resource: "@ManagesBundle/Resources/config/routing.yml"');
delTargetLine(PATH . '/app/config/routing.yml', "prefix:   /manages");


