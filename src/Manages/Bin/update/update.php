#!/usr/bin/env php
<?php


include_once dirname(dirname(__FILE__)) . '/bin/tool.php';

echo_text("框架更新开始，请留意是否出现报错");

$date_dir_path = PATH . '/src/Manages/Bin';

echo exec('wget -c http://oa.jayhe.com/ftp/manages-1.0.tar -P ' . $date_dir_path) ;

$date_file_path = $date_dir_path . '/manages-1.0';

echo exec('tar -xvf '. $date_file_path. '.tar -C '. $date_dir_path);

exec("rm -rf " . $date_file_path . '.tar');

echo_text();

$path_array = read_configure( $date_file_path. '/src/Manages/Bin/update/.update', $date_file_path);

foreach ($path_array as $index => $value){
    $cp_path = str_replace( $date_file_path, PATH , $index);
    $cp_path = str_replace(strrchr($cp_path, '/'), '', $cp_path);
    echo_text( $text = 'cp -rf  ' .  $index . ' ' .  $cp_path);
    echo exec($text);
}

exec("rm -rf " . $date_file_path);

echo_text("框架更新结束！ 如果中途出现错误！请还原代码 -> 排除报错 -> 再重新更新");