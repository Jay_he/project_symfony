<?php
/**
 * 项目工具类[此类规定了代码的基本写法]
 * 
 * 可对核心方法进行重写,基本功能特性不能发生改变,[框架代码更新的时候不会覆盖内容]
 * 
 * User: zmit
 * Date: 6/30/17
 * Time: 9:26 PM
 */

namespace Manages;


use Manages\App\Variable;
use Manages\Solution\Sms\Third\TianYi;

class ToolController extends MastersController
{

    /**
     * 操作主体
     *
     * @var array
     */
    public $subject_type = array(
    );

    /**
     * 得到操作主体
     *
     * @param null $subject_type  static $subject_type_array
     * @param null $subject_id
     * @return array
     */
    public function getSubject($subject_type = null, $subject_id = null)
    {
        if(empty($subject_type)){
            //TODO 重写
        }

        return array(
            'subject_id' => $subject_id,
            'subject_type' => $subject_type,
        );
    }

    /**
     * 设置用户标记
     *
     * @param $user_tag
     * @return mixed
     */
    public function setUserTag($user_tag)
    {

        switch ($this->getPlatform()){
            case $this->getParameter('from_frontend'):
            case $this->getParameter('from_weixin'):
            case $this->getParameter('from_admin');
                $this->get('services.sessions')->set($this->getPlatform() . $this->getParameter('login_tag_session_name'), $user_tag);
                break;
            case $this->getParameter('from_android');
                //TODO 根据业务需求重写
                break;
            case $this->getParameter('from_ios');
                //TODO 根据业务需求重写
                break;
            default:
                $this->ex('来源不正确');
        }

        return $user_tag;
    }

    /**
     * 得到用户标记
     *
     * @param $user_tag
     * @return mixed
     */
    public function getUserTag($user_tag = null)
    {
        switch ($this->getPlatform()){
            case $this->getParameter('from_frontend'):
            case $this->getParameter('from_weixin'):
            case $this->getParameter('from_admin');
                $user_tag = $this->get('services.sessions')->get($this->getPlatform() . $this->getParameter('login_tag_session_name'));
                break;
            case $this->getParameter('from_android');
                //TODO 根据业务需求重写
                break;
            case $this->getParameter('from_ios');
                //TODO 根据业务需求重写
                break;
            default:
                $this->ex('来源不正确');
        }

        return $user_tag;
    }

    /**
     * 移除用户标记
     *
     * @return string|void
     */
    public function removeUserTag(){
        switch ($this->getPlatform()){
            case $this->getParameter('from_frontend'):
            case $this->getParameter('from_weixin'):
            case $this->getParameter('from_admin');
                $this->get('services.sessions')->remove($this->getPlatform() . $this->getParameter('login_tag_session_name'));
                break;
            case $this->getParameter('from_android');
                //TODO 根据业务需求重写
                break;
            case $this->getParameter('from_ios');
                //TODO 根据业务需求重写
                break;
            default:
                $this->ex('来源不正确');
        }
    }
    

    /**
     * 通过来源设置返回类型[特殊情况特殊处理]
     */
    protected function fromSetReturnType()
    {
        if(in_array($this->getPlatform(), Variable::$return_ajax_from)){
            $this->setPlatform($this->getParameter('return_type_ajax'));
        }else{
            $this->setPlatform($this->getParameter('return_type_twig'));
        }
    }

    /**
     *  删除文件回退资源
     *
     * @param $path
     * @param null $tag
     * @param null $tag_id
     */
    public function removeFile($path, $tag = null, $tag_id = null)
    {
        $this->get('services.files')->removeFile($path);
    }

    /**
     * 验证图形验证码
     *
     * @param $session_name
     * @param $salt
     * @param string $error_string
     */
    public function isImgSale($session_name, $salt, $error_string = '图形验证码')
    {
        if(empty($salt)){
            self::$s_verify->setError($error_string . '不能为空');
        }


        if(strtolower($this->get('services.sessions')->get($session_name)) != strtolower($salt)){
            self::$s_verify->setError($error_string . "不正确");
        }

        $this->get('services.sessions')->remove($session_name);
    }

    /**
     * 验证短信验证码
     *
     * @param $phone
     * @param $salt
     * @param $type
     */
    public function isSmsSale($phone, $salt, $type)
    {
        $ali = new TianYi($this->container);
        $r = $ali->is_sale($phone, $salt, $type);
        if(!empty($r)) {
            self::$s_verify->setError($r);
        }
    }
}