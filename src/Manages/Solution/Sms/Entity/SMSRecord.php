<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SMSRecord
 *
 * @ORM\Table(name="sms_record", options={"comment"="短信动态码"}, indexes={@ORM\Index(name="phone", columns={"phone"})})
 * @ORM\Entity
 */
class SMSRecord
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=11, nullable=false, options={"comment"="手机号码"})
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=4, nullable=false, options={"comment"="验证码"})
     */
    private $salt;

    /**
     * @var integer
     *
     * @ORM\Column(name="happen_time", type="integer", nullable=false, options={"comment"="发送时间"})
     */
    private $happenTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="smallint", nullable=false, options={"comment"="短信用途，1：会员注册，2：会员找回密码"})
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="times", type="smallint", nullable=false, options={"comment"="验证码验证次数"})
     */
    private $times;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_used", type="boolean", nullable=false, options={"comment"="验证码是否使用"})
     */
    private $isUsed;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=40, nullable=false, options={"comment"="IP地址"})
     */
    private $ip;

}
