<?php

/**
 * 发送短信[抽象类]
 * 
 * User: zmit
 * Date: 16-11-24
 * Time: 下午9:43
 */

namespace Manages\Solution\Sms;



use Manages\ProjectController;
use Manages\Servers\InputOutPut\Responses;
use Manages\Standard\ThirdParty\InterfaceThirdParty;

abstract class AbstractSms extends ProjectController implements InterfaceThirdParty
{

    /**
     * 发送短信抽象方法
     *
     * @param $phone
     * @param $content
     * @return mixed
     */
    abstract function sendSms($phone, $content);

    /**
     * 短信验证码
     *
     * @param $phone
     * @param $sale
     * @param $type
     * @return string
     */
    public function template($phone, $sale, $type)
    {
        $content = '';

        switch ($type){
            case 1:
                $content = '注册验证码为'.$sale.',请不要告诉他人!';
                break;
            case 2:
                $content = '找回密码验证码为'.$sale.',请不要告诉他人!';
                break;
        }

        return $content;
    }

    /**
     * 短信验证码验证
     *
     * @param $phone
     * @param $sale
     * @param $type
     * @return string
     */
    public function is_sale($phone, $sale, $type)
    {
        if(empty($sale)){
            $result = '短信验证码不能为空';
        }else {
            $salt_info = self::$conn->fetchAssoc("SELECT * FROM sms_record WHERE phone = ? AND type = ? AND salt = ?  AND is_used = 0 LIMIT 1", array($phone, $type, $sale));
            $result = '';
            if ($salt_info['times'] >= 5) {
                $result = '该短信验证码验证次数超过限制';
            } else if ($salt_info['is_used'] == 1) {
                $result = '该短信验证码已被验证';
            } else if ($salt_info['salt'] != $sale) {
                //验证次数加1
                self::$conn->update('sms_record', array('times' => $salt_info['times'] + 1), array('id' => $salt_info['id']));
                $result = '短信验证码不正确';
            } else if (time() - $salt_info['happen_time'] > 1800) {
                $result  = '短信验证码失效';
            } else {
                self::$conn->update('sms_record', array('is_used' => 1), array('id' => $salt_info['id']));
            }
        }

        return $result;
    }

    /**
     * 发送短信验证码
     *
     * @param $phone
     * @param $type
     * @param $is_beta
     * @return array
     */
    public function sendPhoneCode($phone, $type, $is_beta)
    {
        //注销以前的验证码
        self::$conn->executeUpdate("UPDATE sms_record SET happen_time = ? WHERE phone = ? and type = ? and happen_time >= ?", array(time() - 1800,
            $phone, $type, time() - 1800));

        $data['salt'] = $this->get('services.strings')->generateNumberRandom(4);

        //发送短信验证码
        $judge = $this->userMessage($phone, $data['salt'], $type);
        if($judge['errorCode'] != 0){
            return $judge;
        }

        $data['phone'] = $phone;
        $data['happen_time'] = time();
        $data['times'] = 0;
        $data['is_used'] = 0;
        $data['ip'] = $_SERVER['REMOTE_ADDR'];
        $data['type'] = $type;
        self::$conn->insert('sms_record', $data);

        if($is_beta == 0){
            $data['salt'] = '';
        }

        return Responses::arrays('发送成功', $this->getParameter('error_code_success'), array('salt' => $data['salt']));

    }

    /**
     * 发送短信验证码
     *
     * @param $phone
     * @param string $sale  验证码
     * @param $type  1：注册  2；找回密码
     * @return array
     */
    public function userMessage($phone, $sale, $type)
    {
        $content = $this->template($phone, $sale, $type);

        return $this->returnRewrite($this->sendSms($phone, $content));
    }
}
