<?php

/**
 * 潮信
 * User: Jay
 * DateTime: 7/5/1712:02 AM
 */

namespace Manages\Solution\Sms\Third;

use Manages\Servers\InputOutPut\Responses;
use Manages\Solution\Sms\AbstractSms;

class ChaoXin extends AbstractSms
{
    const USERID = '';
    const ACCOUNT = '';
    const PASSWORD = '';

    function returnRewrite($return)
    {
        // TODO: Implement returnRewrite() method.
        $return = (array)$return;

        if(empty($return)){
            return Responses::arrays('发送失败,无法链接短信平台');
        }else{
            if($return['RETURNSTATUS'] == 'Success'){
                return Responses::arrays('发送成功', $this->getParameter('error_code_success'));
            }else{
                return Responses::arrays($return['MESSAGE']);
            }
        }

    }

    function sendSms($phone, $content, $sendtime = '')
    {
        // TODO: Implement sendSms() method.
        $post_data = array();
        $post_data['userid'] = self::USERID;
        $post_data['account'] = self::ACCOUNT;
        $post_data['password'] = self::PASSWORD;
        $post_data['content'] = $content;
        $post_data['mobile'] = $phone;
        $post_data['sendtime'] = $sendtime; //不定时发送，值为''，定时发送，输入格式YYYYMMDDHHmmss的日期值
        $url = 'http://118.145.18.170:8080/sms.aspx?action=send';
        $o = '';
        foreach ($post_data as $k => $v) {
            $o .= "$k=" . urlencode($v) . '&';
        }
        $post = substr($o, 0, -1); // 去除最后一个&符号
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //如果需要将结果直接返回到变量里，那加上这句。
        $xml = curl_exec($ch);

        $xmlparser = xml_parser_create();
        xml_parse_into_struct($xmlparser, $xml, $values);

        xml_parser_free($xmlparser);

        $result = array();
        foreach ($values as $v) {
            if (array_key_exists('tag', $v) && array_key_exists('value', $v)) {
                $result[$v['tag']] = $v['value'];
            }
        }

        return $result;
    }
}