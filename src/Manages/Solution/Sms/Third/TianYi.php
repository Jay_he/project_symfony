<?php
/**
 * 天翼
 * User: Jay
 * DateTime: 7/5/1712:46 AM
 */

namespace Manages\Solution\Sms\Third;


use Manages\Servers\InputOutPut\Responses;
use Manages\Solution\Sms\AbstractSms;

class TianYi extends AbstractSms
{
    const EPID = '';
    const USER_NAME = '';
    const PASSWORD = '';

    function sendSms($phone, $content)
    {
        // TODO: Implement sendSms() method.
        $post_data = array();
        $post_data['epid'] = self::EPID;
        $post_data['User_Name'] = self::USER_NAME;
        $post_data['password'] = self::PASSWORD;
        $post_data['phone'] = $phone;
        $post_data['content'] = $content;
        $post_data['ExtendCode'] = '';
        $url = "http://access.xx95.net:8886/Connect_Service.asmx/SendSmsEx";
        $o = '';
        foreach ($post_data as $k => $v) {
            $o .= "$k=" . urlencode($v) . '&';
        }
        $post = substr($o, 0, -1); // 去除最后一个&符号

        $r = $this->get('services.curl')->request_post($url, $post);

        return $r;
    }

    public function returnRewrite($return)
    {
        // TODO: Implement returnRewrite() method.

        //返回值说明
        /**
         * 00 成功
         * 01 号码（超过上限 50 个）、内容等为空或内容长度超过 210
         * 02 用户鉴权失败
         * 99 服务器接受失败
         * 表现为某个字、词组 内容有屏蔽字
         */
        if (empty($r)) {
            return array();
        } else {
            if (is_string($r)) {
                $r = strip_tags($r);
            } else {
                return Responses::arrays('发送失败,无法链接短信平台');
            }
            if ($r == '00') {
                return Responses::arrays('发送成功', $this->getParameter('error_code_success'));
            } else {
                switch ($r) {
                    case '01':
                        $r = '号码（超过上限 50 个、内容等为空或内容长度超过 210';
                        break;
                    case '02':
                        $r = ' 用户鉴权失败';
                        break;
                    case '99':
                        $r = ' 服务器接受失败';
                        break;
                    default:
                        $r = "内容有屏蔽字" . $r;
                }
                return Responses::arrays($r);
            }
        }

    }

}