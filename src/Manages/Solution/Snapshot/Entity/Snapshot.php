<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Snapshot
 *
 * @ORM\Table(name="snapshot", options={"comment":"快照"})
 * @ORM\Entity
 */
class Snapshot
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="config_key", type="string", length=255, unique=true, options={"comment":"键"})
     */
    private $configKey;

    /**
     * @var string
     *
     * @ORM\Column(name="config_value", type="text", nullable=true, options={"comment":"值"})
     */
    private $configValue;

}
