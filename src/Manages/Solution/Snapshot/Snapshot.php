<?php
/**
 * 快照模块
 */

namespace Manages\Solution\Snapshot;



use Manages\ProjectController;


class Snapshot extends ProjectController
{

    public function setValue($key, $value)
    {
        if(self::$conn->fetchColumn("SELECT id FROM snapshot WHERE config_key = ?", array($key))){
            self::$conn->update('snapshot', array('config_value' => $value), array('config_key' => $key));
        }else{
            self::$conn->insert('snapshot', array('config_key' => $key, 'config_value' => $value));
        }
    }
    
    public function getValue($key, $def = 0)
    {
        self::$conn->fetchColumn("SELECT config_value FROM snapshot WHERE config_key = ?", array($key));

        if(empty($val)){
            $val = $def;
        }

        return $val;
    }
}
