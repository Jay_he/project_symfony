<?php

/**
 * 设置组件
 *
 * User: Jay
 * DateTime: 7/6/1712:03 AM
 */

namespace Manages\Solution\Setting;

use Manages\ProjectController;
use Manages\Servers\InputOutPut\Responses;
use Manages\Standard\Doc\InterfaceBuiltInData;

class Setting extends ProjectController implements InterfaceBuiltInData
{

    /**
     *
     * [
    {
    "config_key": "site_name",  key值
    "default_value": "0",       默认值
    "value_type":"string",      值类型 -> 看set_value()方法的说明
    "value_rule":"255",         值规则 -> 看set_value()方法的说明
    "value_title":"设置项目1",   值标题->用于加载设置项目列表的标题
    "is_show":"1"               是否在设置列表中显示  1显示 0 关闭
    "explain"               设置项说明(选填)
    }
    ]
     *
     */

    /**
     * 得到内容
     *
     * @param $key
     * @return string
     */
    public function getValue($key)
    {
        $value = self::$conn->fetchAssoc("SELECT config_value, value_type FROM settings WHERE config_key = ?", array($key));

        if(empty($value)){
            $info = $this->get('services.files')->readToJson(($this->get('services.files')->
            physicalPathSplicing(array($this->getParameter('manages_dir'), 'Solution', 'Setting', 'Data', 'settings.json'))));
            foreach ($info as $index => $val){
                if($val['config_key'] == $key){
                    $value = array(
                        'config_value' => $val['default_value'],
                        'value_type' => $val['value_type']
                    );
                    break;
                }
            }
            if(empty($value)){
                $value  = array(
                    'config_value' => 0,
                    'value_type' => 'string'
                );
            }
        }

        switch ($value['value_type']) {
            case 'img':
                $value['config_value'] = $this->get('services.files')->getFilePath($value['config_value']);
                break;
        }

        return $value['config_value'];
    }

    /**
     * get_list
     *
     * @param int $is_show
     * @return mixed
     */
    public function getList($is_show = 0)
    {
        $info = $this->get('services.files')->readToJson(($this->get('services.files')->
        physicalPathSplicing(array($this->getParameter('manages_dir'), 'Solution', 'Setting', 'Data', 'settings.json'))));

        foreach ($info as $index => $value){
            if($value['is_show'] == 0 && $is_show == 0){
                unset($info[$index]);
            }
        }

        return $info;
    }

    /**
     * get_info
     *
     * @param $item
     * @return mixed
     */
    public function getInfo($item)
    {
        $info = $this->get('services.files')->readToJson(($this->get('services.files')->
        physicalPathSplicing(array($this->getParameter('manages_dir'), 'Solution', 'Setting', 'Data', 'settings.json'))));

        if(!isset($info[$item])) {
            $this->ex('参数溢出');
        }

        $info =  $info[$item];
        $info['config_value'] = $this->getValue($info['config_key']);

        return $info;
    }

    /**
     * 设置value
     *
     * 此方法说明为整个流程的核心标识
     *
     * value_type
     * string[文本类型]  value_rule：  0|null 代表长文本  其余情况 表明为 最大长度[例如]9999 (不包含边界)
     * int[整数类型]     value_rule:  最小值_最小值[例如] 0_99999999  (包含边界)
     * double[浮点类型]  value_rule:  最小值_最小值_小数点位数[例如] 0_99999999  (包含边界)
     * img[图片路径]     value_rule： 无 (强制为255字符之内的字符串)
     * regular[正则]    value_rule： 需要匹配的正则表达式--正则常量名
     * url             value_rule: 就是规则
     * time            default_value: 00:00:00 时间
     * @param $item
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function setValue($item)
    {
        $config_value = $this->get('utils.requests')->get('config_value', '', true);
        $info = $this->getInfo($item);

        switch ($info['value_type']){
            case 'string':
                if(self::$s_verify->isEmpty($config_value)){
                    self::$s_verify->setError($info['value_title'].'不能为空');
                }

                if(!empty($info['value_rule'])){
                    self::$s_verify->isString($config_value, $info['value_title'], false, $info['value_rule']);
                }
                break;
            case 'number_int':
                $config_value += 0;
                $item = explode('_', $info['value_rule']);
                if($config_value < ($item[0] + 0) || $config_value > ($item[1] + 0)){
                    self::$s_verify->setError($info['value_title']."需在({$item[0]}-{$item[1]})之间");
                }
                break;
            case 'number_double':
                $config_value += 0;
                $item = explode('_', $info['value_rule']);
                if($config_value < ($item[0] + 0) || $config_value > ($item[1] + 0)){
                    self::$s_verify->setError($info['value_title']."需在({$item[0]}-{$item[1]})之间");
                }
                $l = explode('.', $config_value);
                if(isset($l[1])){
                    if(strlen($l[1]) > ($item[2] + 0)){
                        self::$s_verify->setError($info['value_title']."小数点后位数不能超过$item[2]位");
                    }
                }
                break;
            case 'regular':
                self::$s_verify->regexp($config_value, $info['value_title'], false, $info['value_rule']);
                break;
            case 'img':
                self::$s_verify->isString($config_value, '请上传图片', false, 255);
                break;
            case 'url';
                self::$s_verify->isUrl($config_value) ? '' :self::$s_verify->setError("链接格式不正确");
                break;
        }

        if(self::$s_verify->getError()){
            return Responses::arrays(self::$s_verify->getError(), $this->getParameter('error_code_failure'));
        }


        if(self::$conn->fetchColumn("SELECT id FROM settings WHERE config_key = ? ", array($info['config_key']))) {
            self::$conn->update('settings', array('config_value' => $config_value), array('config_key' => $info['config_key']));
        }else{
            $data['config_key'] = $info['config_key'];
            $data['config_value'] = $config_value;
            $data['value_type'] = $info['value_type'];
            self::$conn->insert('settings', $data);
        }

        return Responses::arrays($this->getParameter('message_edit_success'), $this->getParameter('error_code_success'));
    }

    public function builtInData()
    {
        // TODO: Implement builtInData() method.
        $data = $this->container->get('services.files')->
        readToJson($this->get('services.files')->
        physicalPathSplicing(array($this->getParameter('manages_dir'), 'Solution', 'Setting', 'Data', 'settings.json')));

        foreach ($data as $value){
            $info['config_key'] = $value['config_key'];
            $info['config_value'] = $value['default_value'];
            $info['value_type'] = $value['value_type'];

            self::$conn->insert('settings', $info);
        }
    }


}