<?php

/**
 * 省市区信息
 * User: Jay
 * DateTime: 7/5/1711:30 PM
 */

namespace Manages\Solution\Area;
use Manages\ProjectController;
use Manages\Standard\Doc\InterfaceBuiltInData;

class Area extends ProjectController implements InterfaceBuiltInData
{
    /**
     * 得到子地址
     *
     * @param int $parent_id
     * @return array
     */
    public function getSonArea($parent_id = 0)
    {
        return self::$conn->fetchAll("SELECT id, parent_id, name, first_letter FROM area WHERE parent_id = ? AND id <> 0 ORDER BY name ASC , id ASC ", array($parent_id));
    }

    /**
     * 得到全部的城市
     *
     * sort 是否字母归类
     * name 关键词查询
     *
     * @return array
     */
    public function getAllCity()
    {
        $request = $this->get('utils.requests');
        $sort = $request->get('sort', 0);
        $name = $request->get('name', '');


        $where = ' WHERE  parent_id IN (SELECT id FROM area WHERE parent_id = 0 AND id != 0) ';
        if(!$this->get('services.validations')->isEmptyAll($name)){
            $where .= " AND name LIKE '%$name%'";
        }
        $citiesFromDb =  $this->get('database_connection')->fetchAll('SELECT id, name, first_letter FROM area '.$where);
        $cities = array();
        if ( $sort == 1 ) {
            foreach ($citiesFromDb as $value) {
                if(!empty($value['first_letter'])) {
                    $cities[$value['first_letter']][] = array(
                        'id' => $value['id'],
                        'name' => $value['name']
                    );
                }
            }
            ksort($cities);
        } else {
            $cities = $citiesFromDb;
        }

        return $cities;
    }

    /**
     * 得到全部的省市区
     *
     * is_hierarchy 是否层级化
     *
     * @return mixed
     */
    public function getAll()
    {
        $area = self::$conn->fetchAll('SELECT id, parent_id, name FROM area WHERE id != 0');


        if($this->get('utils.requests')->get('is_hierarchy', 0) == 1) {
            $areas = $this->get('services.arrays')->findChildren($area, 'id', 'parent_id');

            return $areas;
        }

        return $area;
    }

    /**
     * 得到地址的详细信息
     *
     * @param $id
     * @return array
     */
    public function getAreaInfo($id)
    {
        return self::$conn->fetchAssoc("SELECT id, parent_id, name, first_letter FROM area WHERE id = ?", array($id));
    }

    /**
     * 得到地址名称
     *
     * @param $id
     * @return mixed
     */
    public function getAreaName($id)
    {
       return self::$conn->fetchColumn("SELECT name FROM area WHERE id = ?", array($id));
    }

    /**
     * 验证省市区是否匹配
     *
     * @param $p_id
     * @param null $c_id
     * @param null $d_id
     * @return bool
     */
    public function isAreaRelation($p_id, $c_id = null, $d_id = null)
    {
        if(!empty($c_id)) {
            if (self::$conn->fetchColumn("SELECT count(id) FROM area WHERE id = ? AND parent_id = ?", array($c_id, $p_id)) < 1) {
                return false;
            }
        }else{
            if(!empty($p_id)) {
                if (self::$conn->fetchColumn("SELECT count(id) FROM area WHERE id = ? ", array($p_id)) < 1) {
                    return false;
                }
            }
        }

        if(!empty($d_id)){
            if(self::$conn->fetchColumn("SELECT count(id) FROM area WHERE id = ? AND parent_id = ?", array($d_id, $c_id)) < 1){
                return false;
            }
        }

        return true;
    }

    /**
     * 地址信息录入
     *
     * @param $data
     * @param $pre
     * @return mixed
     */
    public function areaInfoInput($data, $pre)
    {
        if($data[$pre .'country_id'] == 0){
            //中国
            if($this->isAreaRelation($data[$pre. 'province_id'], $data[$pre. 'city_id'], $data[$pre. 'district_id'])){
               self::$s_verify->setError(
                   '省市区不匹配',
                    array(
                       $pre . 'country_id' => $data[$pre. 'country_id'],
                       $pre . 'province_id' => $data[$pre. 'province_id'],
                       $pre . 'city_id' =>  $data[$pre. 'city_id'],
                       $pre . 'district_id' => $data[$pre. 'city_id']
                    ),
                   $pre . 'area');
            }

            $data[$pre .'country_name'] = '中国';

            if(empty($data[$pre. 'province_id'])){
                $data[$pre. 'province_id'] = null;
                $data[$pre. 'city_id'] = null;
                $data[$pre. 'district_id'] = null;
            }
            if(empty($data[$pre. 'province_id'])){
                $data[$pre. 'city_id'] = null;
                $data[$pre. 'district_id'] = null;
            }
            if(empty($data[$pre. 'district_id'])){
                $data[$pre. 'district_id'] = null;
            }

            $i = 0;
            if (empty($data[$pre . 'province_id'])) {
                $data[$pre . 'province_name'] = null;
                $i++;
            }else{
                $data[$pre. 'province_name'] = $this->getAreaName($data[$pre. 'province_id']);
            }

            if ($data[$pre . 'city_id'] == null) {
                $data[$pre . 'city_name'] = null;
                $i++;
            }else{
                $data[$pre. 'city_name'] = $this->getAreaName($data[$pre. 'city_id']);
            }

            if ($data[$pre . 'district_id'] == null) {
                $data[$pre . 'district_name'] = null;
                $i++;
            }else{
                $data[$pre. 'district_name'] = $this->getAreaName($data[$pre. 'district_id']);
            }

            if(!self::$s_verify->isEmpty($data[$pre. 'address']) && $i == 3){
                self::$s_verify->setError('请完善地址信息', $data[$pre. 'address'], $pre. 'address');
            }

        }else{
            //其他
            $data[$pre. 'country_id'] = null;
            $data[$pre. 'country_name'] = null;
            $data[$pre. 'province_id'] = null;
            $data[$pre. 'city_id'] = null;
            $data[$pre. 'district_id'] = null;
            $data[$pre . 'province_name'] = null;
            $data[$pre . 'city_name'] = null;
            $data[$pre . 'district_name'] = null;
        }

        self::$s_verify->setKey($pre. 'address');
        self::$s_verify->isString($data[$pre. 'address'], '详细地址', true, 255);

        return $data;
    }

    /**
     * 连表查询数据库地址
     *
     * @param $tcn
     * @param $left_join
     * @param string $pre
     * @return array
     */
    public function leftJoinTcn($tcn, $left_join, $pre = '')
    {

        if(strpos($tcn, 'province_id') !== false){
            $tcn .= ", area_p.name as province_name ";
            $left_join .= " LEFT JOIN area area_p on area_p.id = {$pre}province_id ";
        }

        if(strpos($tcn, 'city_id') !== false){
            $tcn .= ", area_c.name as city_name ";
            $left_join .= " LEFT JOIN area area_c on area_c.id = {$pre}city_id ";
        }

        if(strpos($tcn, 'district_id') !== false){
            $tcn .= ", area_d.name as district_name ";
            $left_join .= " LEFT JOIN area area_d on area_d.id = {$pre}district_id ";
        }

        return array($tcn, $left_join);
    }

    public function builtInData()
    {
        // TODO: Implement builtInData() method.
        $data = $this->container->get('services.files')->
        readToJson($this->get('services.files')->
        physicalPathSplicing(array($this->getParameter('manages_dir'), 'Solution', 'Area', 'Data', 'area.json')));

        foreach ($data as $key=>$value){
            self::$conn->insert('area', $value);
        }
    }
}