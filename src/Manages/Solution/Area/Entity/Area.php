<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Area
 *
 * @ORM\Table(name="area", options={"comment":"省市区表"})
 * @ORM\Entity()
 */
class Area
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=10, options={"comment":"省市区编号"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \BaseBundle\Entity\Area
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Area")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, options={"comment":"区域名称"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="first_letter", type="string", nullable=true, length=1, options={"fixed":true, "comment":"区域名称首字母"})
     */
    private $firstLetter;
}
