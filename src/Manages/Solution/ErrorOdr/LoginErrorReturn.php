<?php

/**
 * 登录异常回跳机制
 *
 * User: Jay
 * DateTime: 7/5/171:07 AM
 */
namespace Manages\Solution\ErrorOdr;

use Manages\ProjectController;
use Symfony\Component\Security\Acl\Exception\Exception;

class LoginErrorReturn  extends ProjectController
{
    /**
     * 传参的变量名
     *
     * @var string
     */
    protected $session_login_error_return_name = 'LoginErrorReturn';


    /**
     * 报错
     *
     * @param $message
     * @param string $login_url  登录URL
     * @param bool $return_url  不填则系统自动读取
     * @return array|bool|\Symfony\Component\HttpFoundation\Response
     */
    public function error($message, $login_url , $return_url = false)
    {
        if($this->getReturnType() == $this->getParameter('return_type_twig')){
            $return_info['url'] = $return_url === false ? $this->get('utils.requests')->getCurrentRoute() : $return_url;
            //对post参数进行数据封装--post参数内容不能为数组此类的复杂类型
            if (!empty($_POST)) {
                foreach ($_POST as $index => $value) {
                    $return_info['info'][]['key'] = $index;
                    $return_info['info'][]['value'] = $value;
                }
            }
        }else{
            $return_info['url'] = $return_url === false ? $this->get('utils.requests')->getForwardUrl() : $return_url;
        }

        //对数据进行服务器端保存
        $this->get('services.sessions')->set($this->session_login_error_return_name, $return_info);

        //跳转到登录页面
        if($this->getReturnType() == $this->getParameter('return_type_twig')){
            $this->goUrl($login_url);
            return true;
        }else{
            return $this->response($message, $this->getParameter('error_code_not_logged'));
        }
    }

    /**
     * 得到登录后跳转的URL
     *
     * @param string $get_param
     * @return mixed|string
     */
    public function loginOkUrl($default_url, $get_param = '')
    {
        if(empty($get_param)){
            $get_param = $this->session_login_error_return_name;
        }

        $return_info = $this->get('services.sessions')->get($get_param);

        if (!is_array($return_info)) {
            //清除记录
            $this->get('services.sessions')->remove($get_param);
            return $default_url;
        } else {
            if(array_key_exists('info', $return_info)){
                return $this->getUrl('base_manages_postSubmitHtml', array('key' => $get_param)).'?platform='.$this->getPlatform();
            }else{
                //清除记录
                $this->get('services.sessions')->remove($get_param);
                return $return_info['url'];
            }
        }
    }


    /**
     * 模拟post提交 跳转页面
     *
     * @param $key
     * @return mixed
     */
    public function callback($key)
    {
        $info = $this->get('services.sessions')->get($key);
        $this->get('services.sessions')->remove($key);

        if(!is_array($info)) {
            $this->ex('异常访问！-报错机制');
        }

        if(!array_key_exists('info', $info)){
            $this->ex('异常访问！-报错机制');
        }

        return $info;
    }
}