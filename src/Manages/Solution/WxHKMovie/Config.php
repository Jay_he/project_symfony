<?php

namespace Manages\Solution\WxHKMovie;
use Manages\MastersController;
use Manages\Servers\InputOutPut\Responses;
use Manages\Standard\ThirdParty\InterfaceThirdParty;

/**
 * Created by PhpStorm.
 * User: Jay
 * DateTime: 8/21/177:47 PM
 *
 * 学习地址 https://mp.weixin.qq.com/wiki/1/8a5ce6257f1d3b2afb20f83e72b72ce9.html
 */
class Config extends MastersController implements InterfaceThirdParty
{

    //TODO 接口参数

    protected $app_id = '';

    protected $secret = '';

    protected $token = '';  // 服务器验证token

    protected $encodingaeskey = ''; // 服务器加密解密密钥

    protected $random_code = 'jhjsda79656'; // 随机码--用于验证回调的来源是否正确一致

    //TODO 业务数据

    public $code;

    public $open_id;

    public $access_token;

    public $state;

    public $user_info;


    //TODO 重写返回值

    protected $error_source = '来源错误';
    protected $error_no_response = '未响应';
    protected $error_request_failed = '请求失败';

    public function returnRewrite($return = 0)
    {
        // TODO: Implement returnRewrite() method.
        if($return === 0){
            return Responses::arrays('请求成功', 0);
        }else{
            return Responses::arrays($return, 1);
        }
    }
}