<?php

namespace Manages\Solution\WxHKMovie;


use Manages\Solution\Snapshot\Snapshot;
use Symfony\Component\Security\Acl\Exception\Exception;

class Wx extends Config
{
    /**
     * 验证服务器token
     */
    public function verificationServerToken()
    {
        $request = $this->get('utils.requests');
        $timestamp = $request->get('timestamp');
        $signature = $request->get('signature');
        $nonce = $request->get('nonce');
        $token = $this->token;
        $array = array($timestamp,$nonce,$token);
        sort($array);

        //2.将排序后的三个参数拼接后用sha1加密
        $tmpstr = implode('',$array);
        $tmpstr = sha1($tmpstr);

        //3. 将加密后的字符串与 signature 进行对比, 判断该请求是否来自微信
        if($tmpstr == $signature && !empty($signature))
        {
            exit($request->get('echostr'));
        }

        exit('访问来源不正确');
    }


    /**
     * 得到请求登录授权的url
     *
     * @param $redirect_url
     * @param string $state  重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
     * @param string $scope  snsapi_base | snsapi_userinfo
     * @return string
     */
    public function getAuthorizationRoute($redirect_url , $state = '', $scope = 'snsapi_userinfo')
    {
        //设置第三方授权接口参数
        $appid = $this->app_id;
        $redirect_url = urlencode($redirect_url);
        $response_type = 'code';
        $state = $state  . $this->random_code;

        //拼接url，跳转到授权页面
        return "https://open.weixin.qq.com/connect/oauth2/authorize?appid=$appid&redirect_uri=$redirect_url&response_type=$response_type&scope=$scope&state=$state#wechat_redirect";

    }

    /**
     * 授权回调处理并且得到code
     *
     * @return int
     */
    public function authorizedReboundProcessingGetCode()
    {
        $r = $this->verifyState();
        if($r['errorCode'] == 1){
            return $r;
        }

        //判断有没有授权成功
        if (!array_key_exists('code', $_GET)) {
            return $this->returnRewrite('授权失败');
        } else {
            //通过code值换取网页授权access_token
            $this->code = $_GET['code'];
            return $this->returnRewrite();
        }
    }

    /**
     * 得到 AccessToken
     *
     * @return int
     */
    public function getAccessToken($type = 'public')
    {
        $m_snapshot = new Snapshot($this->container);

        if($type == 'public') {
            $this->access_token = $m_snapshot->getValue('wx_public_access_token');
            $access_token_end_time = $m_snapshot->getValue('wx_public_access_token_end_time');
            if(empty($this->access_token)  || empty($access_token_end_time) ||time() > $access_token_end_time) {
                $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$this->app_id.'&secret='.$this->secret;
            }else {
                return $this->returnRewrite();
            }
        }elseif($type == 'oath2'){
            $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=$this->app_id&secret=$this->secret&code=$this->code&grant_type=authorization_code";
        }else{
            throw new Exception('type溢出');
        }

        $return = json_decode($this->get('services.curl')->request_get($url), true);



        if(empty($return) || !is_array($return)){
            return $this->returnRewrite($this->error_no_response);
        }

        if(!isset($return['access_token'])){
            return $this->returnRewrite($return['errmsg']);
        }

        $this->access_token = $return['access_token'];

        switch ($type) {
            case 'public':
                $expires_in = time() + $return['expires_in'] - 30;
                $m_snapshot->setValue('wx_public_access_token', $this->token);
                $m_snapshot->setValue('wx_public_access_token_end_time', $expires_in);
                break;
            case 'oath2':
                $this->open_id = $return['openid'];
                if(empty($this->open_id)){
                    return $this->returnRewrite('授权失败，请重新扫码');
                }
                break;
        }

        return $this->returnRewrite();
    }


    /**
     * 得到用户信息
     *
     * @return int
     */
    public function getUserInfo()
    {
        $url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$this->access_token.'&openid='.$this->app_id;
        //链接平台　得到用户的基本信息
        $return = json_decode($this->get('services.curl')->request_get($url), true);

        if(empty($return) || !is_array($return)){
            return $this->returnRewrite($this->error_no_response);
        }

        if(!isset($return['openid'])){
            return $this->returnRewrite($return['errmsg']);
        }

        if(empty($return['headimgurl'])){
            $user_info['headimgurl'] = '';
        }

        if(array_key_exists('nickname', $return)){
            $return['nickname'] = $this->get('services.strings')->encodeContentWithEmoticon($return['nickname'], true); //保存表情到数据库--转义
        }

        $this->user_info = $return;

        return $this->returnRewrite();
    }

    //TODO 基础方法

    /**
     * 验证State
     *
     * @return array|int|mixed|\Symfony\Component\HttpFoundation\Response
     */
    private function verifyState()
    {
        $state = self::$s_request->get('state', '');
        if(empty($state) || strpos($_GET['state'], $this->random_code) === false){
            return $this->returnRewrite($this->error_source);
        }

        $this->state = rtrim($state, $this->random_code);

        return $this->returnRewrite();
    }

}