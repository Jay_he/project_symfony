<?php

/**
 * Redis [此模块尚未集成结束]
 *
 * User: Jay
 * DateTime: 7/6/1712:31 AM
 */

namespace Manages\Solution\Redis;

use JPush\Client;
use Manages\ProjectController;

class Redis extends ProjectController
{
    const PRE_SESSION = 'SESSION';

    const SESSION_EFFECTIVETIME = '99999999'; //毫秒为单位

    private static $redis = null;


    /**
     * @return Client
     */
    public function getSerever()
    {
        return self::$redis;
    }

    public function Client()
    {
        self::$redis = new Client(
            array(
                'scheme' => 'tcp',
                'host' => 'localhost',
                'port' => '6379',
                'password' => '123456',
            )
        );

//        $this->get('snc_redis.default');
    }

    /****************Session 模块的技术实现  start *************************************/

    /**
     * 得到Session的前缀
     *
     * @return string
     */
    public function getSessionPre()
    {
        return self::PRE_SESSION . $this->getParameter('prefix_session') . session_id() . '_';
    }

    /**
     * 设置Session
     *
     * @param $key
     * @param $value
     */
    public function setSession($key, $value)
    {
        $this->getSerever()->set($this->getSessionPre(). $key, $value);
        $this->setSessionEffectiveTime();
    }

    /**
     * 得到Session
     *
     * @param $key
     * @return string
     */
    public function getSession($key)
    {
        $this->setSessionEffectiveTime();
        return $this->getSerever()->get($this->getSessionPre() .$key);
    }

    /**
     * 清除Session
     */
    public function clearSession()
    {
        $this->getSerever()->del($this->getAllSession());
    }

    /**
     * 设置Session的时间
     */
    public function setSessionEffectiveTime($key = '')
    {
        if(empty($key)) {
            foreach ($this->getAllSession() as $val) {
                $this->getSerever()->pexpire($val, self::SESSION_EFFECTIVETIME);
            }
        }else{
            $this->getSerever()->pexpire($this->getSessionPre() . $key, self::SESSION_EFFECTIVETIME);
        }
    }

    /**
     * 得到单个Session的时间
     *
     * @param $key
     * @return int
     */
    public function getSessionEffectiveTime($key)
    {
        return $this->getSerever()->pttl($this->getSessionPre() .$key);
    }

    /**
     * 移除Session
     *
     * @param $key
     */
    public function removeSession($key)
    {
        $this->getSerever()->del(array($this->getSessionPre(). $key));
    }

    /**
     * 得到全部Session
     *
     * @return array
     */
    public function getAllSession()
    {
        return $this->getSerever()->keys($this->getSessionPre() .'*');

    }

    /**
     * 得到全部SessionValue
     *
     * @return array
     */
    public function getAllSessionValue()
    {
        $data = array();
        foreach ($this->getAllSession() as $index => $value){
            $data[$value] = $this->getSerever()->get($value);
        }

        return $data;
    }


    /**************** Session 模块的技术实现  end *************************************/


    /****************   第二种研究方向  并发方向  ************************************/
}