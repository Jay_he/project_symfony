<?php

namespace AdminBundle\Controller;

use Manages\ProjectController;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminController extends ProjectController
{
    public function indexAction()
    {
        return $this->render('@Admin/index.html.twig');
    }

    public function __construct(ContainerInterface $container = null)
    {
        parent::__construct($container);
        $this->setPlatform('admin');
        $this->setReturnType('twig');
    }
}
