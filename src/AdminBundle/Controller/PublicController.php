<?php

namespace AdminBundle\Controller;

use Manages\Servers\Tool\Systems;

class PublicController extends AdminController
{
    /**
     * 删除
     *
     * @return array|bool|object|\Symfony\Component\HttpFoundation\Response
     */
    public function delAction()
    {
        $r = $this->inlet('ajax', true);
        if($r !== true){
            return $r;
        }

        $id = self::$s_request->get('select_item');

        if(empty($id)){
            return $this->response('请选择删除项目');
        }

        if(!is_array($id)){
            $id = array($id);
        }

        foreach ($id as $index=> $value) {
            switch (self::$s_admin->getMark()){
                default:
                    return $this->response('类型溢出');
            }
        }

        return $this->response('删除成功', 0,
            [
                'go_url' => self::$s_admin->getReturnUrl()
            ]);
    }

    /**
     * 导入
     *
     * @return array|bool|object|\Symfony\Component\HttpFoundation\Response
     */
    public function importAction()
    {
        $r = $this->inlet('ajax', true);
        if($r !== true){
            return $r;
        }

        $path = self::$s_request->get('path');

        if(empty($path)){
            return $this->response('请上传表格');
        }

        $path = implode('/', [Systems::rootdir(), 'web', $path]);


        //实例化PHPExcel类
        $PHPReader = new \PHPExcel_Reader_Excel2007();
        if (!$PHPReader->canRead($path)) {
            $PHPReader = new \PHPExcel_Reader_Excel5();
            if (!$PHPReader->canRead($path)) {
                return $this->response('数据格式不正确');
            }
        }
        //设置只读，可取消类似"3.08E-05"之类自动转换的数据格式，避免写库失败
        $PHPReader->setReadDataOnly(true);
        //读取Excel文件
        $PHPExcel = $PHPReader->load($path);
        //读取excel文件中的第一个工作表
        $sheet = $PHPExcel->getSheet(0);
        //取得最大的列号
        $allColumn = $sheet->getHighestColumn();
        //取得最大的行号
        $allRow = $sheet->getHighestRow();

        switch (self::$s_admin->getMark()){
            //TODO 示例可删除
            case 'user':
                //从第二行开始插入,第一行是列名
                for ($currentRow = 2; $currentRow <= $allRow; $currentRow++) {
                    $value = $PHPExcel->getActiveSheet()->getCell("A" . $currentRow)->getValue();
                }
                break;
            default:
                return $this->response('类型溢出');

        }

        return $this->response('导入成功', 0,
            [
                'go_url' => self::$s_admin->getReturnUrl()
            ]);
    }
}
