(function($){
    $.fn.extend({ 
        //插件名称 - select
        checkboxFrame: function(options) {

            //默认参数
            var defaults = {
                
            };
            
            var options = $.extend({},defaults, options);
            var _this = $(this);

            function checkboxList(){
                var thisClass = _this.attr('class');

                _this.wrap('<div class="select-wrap controls class7"></div>')
                var html = '';
                html += '<div class="select-cont">';
                html += '<div class="'+thisClass+'">请选择<span class="triangle-down"></span></div>';
                html += '<ul class="select-list" style="display: none;">';

                $('option',_this).each(function(){
                    var val = $(this).val();
                    var text = $(this).text();
                    html += '<li data-val="'+val+'" class="select-item">'+text+'</li>';
                })

                html += '</ul>';
                html += '</div>';

                $('.select-wrap').append(html);
            }

            function checkboxCont(){
                _this.hide();
                _this.attr('multiple','multiple');

                selectList();

                $(".select-input").click(function(event){
                    $('.select-list').toggle();
                    event.stopPropagation();
                })

                $('.select-list').click(function(event){
                     event.stopPropagation();
                })

                $(document).click(function(){
                    $(".select-list").hide();
                })

                $('.select-item').click(function(){

                    var cont = $(this).text();
                    var liVal = $(this).data('val');
                    if (!$(this).hasClass('select-active')) {
                        $(this).addClass('select-active');
                        $('#cont').append('<span class="select-text">'+cont+'<a data-val="'+liVal+'" class="select-delete">-</a></span>');

                        $('option',_this).each(function(){

                            var optVal = $(this).val();
                            if (optVal == liVal) {
                                $(this).attr('selected',true);
                            }
                        })
                    }
                })

                            
                $('body').on('click','.select-delete',function(){

                    $(this).parent().remove();
                    var thisVla = $(this).data('val');

                    $('.select-list .select-item.select-active').each(function(){
                        // alert(666)
                        var liVal = $(this).data('val');
                        if (thisVla == liVal) {
                            $(this).removeClass('select-active');
                        }
                    })

                    $('option',_this).each(function(){
                    
                        var optVal = $(this).val();
                        if (optVal == thisVla) {
                            $(this).attr('selected',false);

                        }
                    })
                })
            }
        
            return this.each(function() {
                checkboxCont()
            });

        }
    });

}(jQuery));

$(function(){
    $('[data-select=select]').checkboxFrame();
})

