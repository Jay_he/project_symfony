(function($){
    $.fn.extend({
        //插件名称 - select
        areaLinkage: function(options) {

            //默认参数
            var defaults = {
                url:'',                        //获取全部省市区参数的路由
                province_id:'',                //默认选中时的省份id
                city_id:'',                    //默认选中时的市份id
                county_id:''                   //默认选中时的区/县id
            };

            var options = $.extend({},defaults, options);
            var opt = options;
            var _that = this;

            function area() {
                var areaAll;
                var num = 0;
                // $.ajax({
                //     url: opt.url,
                //     data:{is_hierarchy:0 },
                //     type:'POST',
                //     success:function (data) {
                //         areaAll = data.data.info;
                //    // console.log(_that);
                //         var id = opt.province_id;
                //         var parent_id = 0;
                //         var _this = $('.province',_that);
                //         getArea(id,parent_id,_this);
                //     },
                //     error:function () {
                //         alert('获取地区失败，您可能遇到了网络问题，请稍后重试')
                //     }
                // });
                $.getJSON(opt.url,{is_hierarchy:0 },function (data) {
                    areaAll = data.data.info;
                    // console.log(_that);
                    var id = opt.province_id;
                    var parent_id = 0;
                    var _this = $('.province',_that);
                    getArea(id,parent_id,_this);
                });

                $('.province',_that).change(function () {
                    var id = opt.city_id;
                    var parent_id = $('option:selected',this).val();
                    var _this = $(this).parents('.controls').find('.city');
                    $(this).parents('.controls').find('.county').children('option:not(:eq(0))').remove();
                    getArea(id,parent_id,_this);
                });

                $('.city',_that).change(function () {
                    var id = opt.county_id;
                    var parent_id = $('option:selected',this).val();
                    var _this = $(this).parents('.controls').find('.county');
                    getArea(id,parent_id,_this);
                });

                function getArea(id,parent_id,_this) {
                    if(num >= 2){
                        id = '';
                    }

                    var html = '';
                    for(var i = 0,len = areaAll.length; i < len; i++){
                        if(areaAll[i]['parent_id'] == parent_id){

                            if(areaAll[i]['id'] == id){
                                html += '<option value="'+areaAll[i]['id']+'" selected>'+areaAll[i]['name']+'</option>';
                            }else{
                                html += '<option value="'+areaAll[i]['id']+'">'+areaAll[i]['name']+'</option>';
                            }
                        }
                    }
                    _this.children('option:not(:eq(0))').remove();
                    _this.append(html);

                    if(num < 2){
                        if (id != '') {
                            _this.trigger('change')
                            num++;
                        }
                    }
                }
            }

            return this.each(function() {
                area();
            });

        }
    });

}(jQuery));


