

// new responseForm()        //参数是必传的，是你要自适应表单的最外层class，最好是form标签的class

(function($){
    $.fn.extend({
        //插件名称 - selectAjax
        responseForm: function(options) {

            //默认参数
            var defaults = {
                clearfixEle:[],                  //填写需要换行的元素的class或id名，用数组形式写如['.class','#id']
                spaceCol:{}                      //填写需要空单元的元素的class或id名和要空几个单元格，用数组形式写如{'.class':'5','#id':'6'}
            };

            var options = $.extend({},defaults, options);
            var _this=$(this);

            function responsef() {

                $(' .controls',_this).css('marginLeft','120px');

                $(' .controls input',_this).css({
                    'width':'calc(100% - 14px)',
                    'max-width':'157px'
                });

                $(' .controls select',_this).css({
                    'width':'100%',
                    'max-width':'171px'
                });

                $(' .control-label',_this).css({'width': '110px','text-align':'right'});

                $(' .controls',_this).each(function(index){

                    $(this).addClass('class'+index);
                    $(this).prev('.control-label').addClass('class'+index);

                    $('.class'+index).wrapAll('<div class="layui-col-md6 layui-col-lg3" style="margin-bottom: 20px;"></div>');
                });

                $(' .controls input[type=hidden]',_this).each(function () {
                    $(this).parents(_this).css('marginBottom','0')
                });

                $('.layui-col-md6').wrapAll('<div class="container-fluid"><div class="layui-row"></div></div>');

            }


            function clearfix(ele) {

                for(var i = 0;i < ele.length;i++){
                    $(ele[i]).parents('.layui-col-md6.layui-col-lg3').after('<div class="clearfix"></div>')

                }

            }

            function spaceCol(space) {

                for(var json in space){
                    var num = space[json] * 3;

                    $(json).parents('.layui-col-md6.layui-col-lg3').addClass('layui-col-lg-offset'+num)

                }

            }

            return this.each(function() {
                var o = options;
                responsef();
                clearfix(o.clearfixEle);
                spaceCol(o.spaceCol);
            });

        }
    });
}(jQuery));

$(function () {
    $('#form').responseForm();
});

jQuery.prototype.serializeObject = function() {
    var a,o,i,e;
    a=this.serializeArray();
    o={};
    for(i=0;i<a.length;i++){
        e=a[i];
        if(o[e.name] !== undefined){
            if(!o[e.name].push){
                o[e.name] = [o[e.name]];
            }
            o[e.name].push(e.value || '');
        }else{
            o[e.name]=e.value || '';
        }
    }
    return o;
};