$(function () {
    setTimeout(function () {
        var w_height=$(window).height();
        var w_width=$(window).width();
        var div_height=$("#main-content").children(".container-fluid").height();
        var header_height=$("#header").outerHeight();
        if (w_width>979) {
            header_height = 65;
        }else if(w_width>767 && w_width<979){
            header_height = 75;
        }else{
            header_height=80;
        }
        var cont_w_height=w_height - 33 - header_height;
        if(div_height < cont_w_height){
            $("#main-content").css("min-height",cont_w_height);
        }else{
            $("#main-content").css("min-height",div_height);
        }
        $(window).resize(function () {
            w_height=$(window).height();
            w_width=$(window).width();
            div_height=$("#main-content").children(".container-fluid").height();
            header_height=$("#header").height();
            if (w_width>979) {
                header_height = 65;
            }else if(w_width>767 && w_width<979){
                header_height = 75;
            }else{
                header_height=80;
            }
            cont_w_height=w_height - 33 - header_height;
            if(div_height < cont_w_height){
                $("#main-content").css("min-height",cont_w_height);
            }else{
                $("#main-content").css("min-height",div_height);
            }
        })
    },500)
})