/**
 * 返回值预处理
 *
 *
 * @param result
 * @param go_url
 * @param succeed
 */
function resultPreprocess(result, go_url, succeed) {
    windowsMessage(result.message.message, function () {
        if(go_url != undefined && result.errorCode == 0){
            window.location.href = go_url;return;
        }

        if(result.data.go_url != undefined){
            window.location.href = result.data.go_url;return;
        }

        if(succeed){
            succeed(result);
        }
    }, result.errorCode);
}

/**
 *  消息框
 *
 * @param message
 * @param succeed
 * @param errorCode
 */
function windowsMessage(message, succeed, errorCode) {
    if(errorCode != 0) {
        layer.open(
            {
                title: '提示',
                type: 0,
                content: message,
                end: succeed
            }
        );
    }else {
        layer.msg(message,{time:1000}, succeed)
    }
}


/**
 * 确认框
 *
 * @param message
 * @param succeed
 */
function windowsConfirm(message, succeed) {
    layer.confirm(message,{
        title: '提示',
        btn: ['确认', '取消']
    },
    succeed,
    function (index, layero) {
       layer.close();
    }
    );
}

/**
 * 展示框
 *
 *
 * @param Html
 */
function windowsDisplay(Html) {

}


/**
 * 分页数据溢出策略
 *
 * @param page
 * @param count
 * @param rows
 * @param data_len
 */
function pageDataPrimary(page, count, rows, data_len) {
    if(page != 1 && data_len == 0 && page != ''){
        go_page = 1;
        if(count > rows){
            go_page = Math.ceil(count/rows);
        }

        window.location.href = window.location.href.replace('page=' + page, 'page=' + go_page);
    }
}

/**
 *  打开弹窗
 *
 * @param pop_id
 */
function open_popup(pop_id) {
    $('#'+ pop_id).css('display', 'block');
    $('#windows' + pop_id).css('display', 'block');
}

/**
 *  关闭弹窗
 *
 * @param pop_id
 */
function close_popup(pop_id) {
    $('#'+ pop_id).css('display', 'none');
    $('#windows' + pop_id).css('display', 'none');
}