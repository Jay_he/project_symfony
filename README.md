# Manages 1.1 Jay 

## 项目初始化
cp app/config/parameters.yml.ref app/config/parameters.yml

## 数据库、数据表创建

php app/console doctrine:database:create
php app/console doctrine:schema:create

## 文件权限目录 --读写权限

chmod -R 777 app/cache app/logs web/upload 

## 内置数据

php app/console doctrine:fixtures:load  --append

## php.ini

允许上传的文件大小 : 10M

## 定时任务

## 第三方目录权限

## 其他

## 更新记录



